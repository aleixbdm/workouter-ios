//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol TokenRefresherProtocol: class {
    init(modelController: ModelController, networkController: NetworkController)
    func refreshToken(onSuccess: @escaping () -> (), onError: @escaping (Error) -> ())
}

final class TokenRefresher: TokenRefresherProtocol {
    private let modelController: ModelController
    private let networkController: NetworkController
    
    init(modelController: ModelController, networkController: NetworkController) {
        self.modelController = modelController
        self.networkController = networkController
    }
}

// MARK: Refresh token
extension TokenRefresher {
    func refreshToken(onSuccess: @escaping () -> (), onError: @escaping (Error) -> ()) {
        guard let userId = self.modelController.userDefaults.auth?.userId else {
            // TODO: Handle logout
            return
        }
        let params: NetworkParameters = [
            .body : [
                .userId(userId)
            ]
        ]
        self.networkController.refreshToken(params: params) { (authResponse, error) in
            if let authSchema = authResponse?.authSchema {
                Logger.log("refreshed accessToken: \(authSchema.accessToken)")
                self.saveAuth(authSchema)
                onSuccess()
            } else if let error = error {
                Logger.log("error: \(error)")
                onError(error)
            }
        }
    }
}

// MARK: Private methods
extension TokenRefresher {
    private func saveAuth(_ authSchema: AuthSchema) {
        self.modelController.userDefaults.saveAuth(authSchema)
    }
}
