//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol DataRefresherHandlerWithParamProtocol: class {
    func assignCall(_ call: @escaping (String) -> (), param: String) -> ()
    func refresh()
    func stop()
}

final class DataRefresherHandlerWithParam: DataRefresherHandlerWithParamProtocol {
    private var refreshing: Bool = false
    private var timer: Timer?
    private var call: ((String) -> ())?
    private var param: String?
}

// MARK: Assign Call
extension DataRefresherHandlerWithParam {
    func assignCall(_ call: @escaping (String) -> (), param: String) -> () {
        guard self.call == nil else { return }
        self.call = call
        self.param = param
    }
}

// MARK: Refresh
extension DataRefresherHandlerWithParam {
    func refresh() {
        guard !self.refreshing && self.call != nil else { return }
        self.refreshing = true
        self.timer = Timer.scheduledTimer (
            timeInterval: RefreshInterval, target: self,
            selector: #selector(self.doCall),
            userInfo: nil, repeats: true
        )
    }

    @objc private func doCall() {
        guard let call = self.call, let param = self.param else { return }
        call(param)
    }
}

// MARK: Stop
extension DataRefresherHandlerWithParam {
    func stop() {
        self.timer?.invalidate()
        self.refreshing = false
        self.call = nil
    }
}
