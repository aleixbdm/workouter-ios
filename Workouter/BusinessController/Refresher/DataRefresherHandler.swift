//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol DataRefresherHandlerProtocol: class {
    func assignCall(_ call: @escaping () -> ()) -> ()
    func refresh()
    func stop()
}

final class DataRefresherHandler: DataRefresherHandlerProtocol {
    private var refreshing: Bool = false
    private var timer: Timer?
    private var call: (() -> ())?
}

// MARK: Assign Call
extension DataRefresherHandler {
    func assignCall(_ call: @escaping () -> ()) -> () {
        guard self.call == nil else { return }
        self.call = call
    }
}

// MARK: Refresh
extension DataRefresherHandler {
    func refresh() {
        guard !self.refreshing && self.call != nil else { return }
        self.refreshing = true
        self.timer = Timer.scheduledTimer (
            timeInterval: RefreshInterval, target: self,
            selector: #selector(self.doCall),
            userInfo: nil, repeats: true
        )
    }

    @objc private func doCall() {
        guard let call = self.call else { return }
        call()
    }
}

// MARK: Stop
extension DataRefresherHandler {
    func stop() {
        self.timer?.invalidate()
        self.refreshing = false
        self.call = nil
    }
}
