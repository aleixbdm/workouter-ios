//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol DataRefresherProtocol: class {
    var refreshChatsHandler: DataRefresherHandler { get }
    var refreshChatMessagesHandler: DataRefresherHandlerWithParam { get }
    func assignRefreshChatsCall(_ call: @escaping () -> ())
    func assignRefreshChatMessagesCall(_ call: @escaping (String) -> (), chatId: String)
    func refreshChats()
    func refreshChatMessages()
    func stopRefreshChats()
    func stopRefreshChatMessages()
}

final class DataRefresher: DataRefresherProtocol {
    private(set) var refreshChatsHandler: DataRefresherHandler
    private(set) var refreshChatMessagesHandler: DataRefresherHandlerWithParam

    init() {
        self.refreshChatsHandler = DataRefresherHandler()
        self.refreshChatMessagesHandler = DataRefresherHandlerWithParam()
    }
}

// MARK: Assign refresh chats call
extension DataRefresher {
    func assignRefreshChatsCall(_ call: @escaping () -> ()) {
        self.refreshChatsHandler.assignCall(call)
    }
}

// MARK: Assign refresh chat messages call
extension DataRefresher {
    func assignRefreshChatMessagesCall(_ call: @escaping (String) -> (), chatId: String) {
        self.refreshChatMessagesHandler.assignCall(call, param: chatId)
    }
}

// MARK: Refresh chats
extension DataRefresher {
    func refreshChats() {
        self.refreshChatsHandler.refresh()
    }
}

// MARK: Refresh chat messages
extension DataRefresher {
    func refreshChatMessages() {
        self.refreshChatMessagesHandler.refresh()
    }
}

// MARK: Stop refresh chats
extension DataRefresher {
    func stopRefreshChats() {
        self.refreshChatsHandler.stop()
    }
}

// MARK: Stop refresh chat messages
extension DataRefresher {
    func stopRefreshChatMessages() {
        self.refreshChatMessagesHandler.stop()
    }
}
