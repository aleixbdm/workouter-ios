//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol ProfileControllerProtocol: class {
    var auth: Auth? { get }
    var settings: Settings? { get }
    func login(email: String, password: String)
    func loginWithFB(accessToken: String)
    func register(email: String, password: String)
    func fetchProfile()
    func createProfile(name: String, firstName: String, imageUrls: [String], age: Int, gender: Gender, preference: Preference, city: String)
    func logout()
}

final class ProfileController: ControllerType, ProfileControllerProtocol {
    private(set) var tokenRefresher: TokenRefresher
    private let modelController: ModelController
    private let networkController: NetworkController

    init(modelController: ModelController, networkController: NetworkController) {
        self.modelController = modelController
        self.networkController = networkController
        self.tokenRefresher = TokenRefresher(modelController: self.modelController, networkController: self.networkController)
    }
}

// MARK: Auth
extension ProfileController {
    var auth: Auth? {
        return self.modelController.userDefaults.auth
    }
}

// MARK: Settings
extension ProfileController {
    var settings: Settings? {
        return self.modelController.userDefaults.settings
    }
}

// MARK: Login
extension ProfileController {
    func login(email: String, password: String) {
        let params: NetworkParameters = [
            .body : [
                .email(email),
                .password(password)
            ]
        ]
        self.callNetwork(self.networkController.login, params: params, onSuccess: { authResponse in
            if let authSchema = authResponse.authSchema {
                self.saveAuth(authSchema)
                self.fetchProfile()
            }
        }, onError: { error in
            self.saveProfileError(error)
        })
    }
}

// MARK: Login with fb
extension ProfileController {
    func loginWithFB(accessToken: String) {
        let params: NetworkParameters = [
            .body : [
                .accessToken(accessToken)
            ]
        ]
        self.callNetwork(self.networkController.loginWithFB, params: params, onSuccess: { authResponse in
            if let authSchema = authResponse.authSchema {
                self.saveAuth(authSchema)
                self.fetchProfile()
            }
        }, onError: { error in
            self.saveProfileError(error)
        })
    }
}

// MARK: Register
extension ProfileController {
    func register(email: String, password: String) {
        let params: NetworkParameters = [
            .body : [
                .email(email),
                .password(password)
            ]
        ]
        self.callNetwork(self.networkController.register, params: params, onSuccess: { authResponse in
            if let authSchema = authResponse.authSchema {
                self.saveAuth(authSchema)
                self.fetchProfile()
            }
        }, onError: { error in
            self.saveProfileError(error)
        })
    }
}

// MARK: Fetch profile
extension ProfileController {
    func fetchProfile() {
        self.loadProfile()
        self.callNetwork(self.networkController.fetchProfile, params: nil, onSuccess: { profileResponse in
            if let profileSchema = profileResponse.profileSchema {
                self.saveProfile(profileSchema)
            }
        }, onError: { error in
            self.saveProfileError(error)
        })
    }
}

// MARK: Create profile
extension ProfileController {
    func createProfile(name: String, firstName: String, imageUrls: [String], age: Int, gender: Gender, preference: Preference, city: String) {
        let params: NetworkParameters = [
            .body : [
                .name(name),
                .firstName(firstName),
                .imageUrls(imageUrls),
                .age(age),
                .gender(gender.rawValue),
                .preference(preference.rawValue),
                .city(city),
            ]
        ]
        self.callNetwork(self.networkController.createProfile, params: params, onSuccess: { profileResponse in
            if let profileSchema = profileResponse.profileSchema {
                self.saveProfile(profileSchema)
            }
        }, onError: { error in
            self.saveProfileError(error)
        })
    }
}

// MARK: Logout
extension ProfileController {
    func logout() {
        self.modelController.userDefaults.deleteAll()
        self.modelController.repository.deleteAll()
    }
}

// MARK: Private methods
extension ProfileController {
    private func saveAuth(_ authSchema: AuthSchema) {
        self.modelController.userDefaults.saveAuth(authSchema)
    }

    private func loadProfile() {
        self.modelController.repository.profileRepository.loadProfile()
    }
    
    private func saveProfile(_ profileSchema: ProfileSchema) {
        self.modelController.repository.profileRepository.saveProfile(profileSchema)
        if let settingsSchema = profileSchema.settings {
            self.saveSettings(settingsSchema)
        }
    }

    private func saveSettings(_ settingsSchema: SettingsSchema) {
        self.modelController.userDefaults.saveSettings(settingsSchema)
    }

    private func saveProfileError(_ error: Error) {
        self.modelController.repository.profileRepository.saveProfileError(error)
    }
}
