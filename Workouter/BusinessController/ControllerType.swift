//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol ControllerType {
    var tokenRefresher: TokenRefresher { get }
    init(modelController: ModelController, networkController: NetworkController)
    func refreshToken(onSuccess: @escaping () -> (), onError:  @escaping (Error) -> ())
    func callNetwork<T>(_ networkCall: @escaping (NetworkParameters?, @escaping (T?, Error?) ->()) -> Void, params: NetworkParameters?, onSuccess: @escaping (T) ->(), onError: @escaping (Error) ->()) -> ()
}

// MARK: Refresh token
extension ControllerType {
    func refreshToken(onSuccess: @escaping () -> (), onError:  @escaping (Error) -> ()) {
        self.tokenRefresher.refreshToken(onSuccess: onSuccess, onError: onError)
    }
}

// MARK: Call network
extension ControllerType {
    func callNetwork<T>(_ networkCall: @escaping (NetworkParameters?, @escaping (T?, Error?) ->()) -> Void, params: NetworkParameters? = nil, onSuccess: @escaping (T) ->(), onError: @escaping (Error) ->()) -> () {
        networkCall(params) { response, error in
            if let error = error {
                if let meetDayError = error as? MeetDayError {
                    switch meetDayError {
                    case .authError:
                        self.refreshToken(onSuccess: {
                            // Repeat call to server
                            self.callNetwork(networkCall, params: params, onSuccess: onSuccess, onError: onError)
                        }, onError: { error in
                            onError(error)
                        })
                        return
                    default: break
                    }
                }
                Logger.log("error: \(error)")
                onError(error)
            } else if let response = response {
                onSuccess(response)
            }
        }
    }
}
