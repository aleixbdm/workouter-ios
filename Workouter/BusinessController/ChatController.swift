//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol ChatControllerProtocol: class {
    func fetchChats()
    func fetchChatMessages(chatId: String)
    func sendChatMessage(chatId: String, messageText: String)
    func stopRefreshChats()
    func stopRefreshChatMessages()
    func discardChatMessages()
}

final class ChatController: ControllerType, ChatControllerProtocol {
    private(set) var tokenRefresher: TokenRefresher
    private let modelController: ModelController
    private let networkController: NetworkController
    private let dataRefresher: DataRefresher
    
    init(modelController: ModelController, networkController: NetworkController) {
        self.modelController = modelController
        self.networkController = networkController
        self.tokenRefresher = TokenRefresher(modelController: self.modelController, networkController: self.networkController)
        self.dataRefresher = DataRefresher()
    }
}

// MARK: Fetch chats
extension ChatController {
    func fetchChats() {
        self.dataRefresher.assignRefreshChatsCall(self.fetchChats)
        self.callNetwork(self.networkController.fetchChats, params: nil, onSuccess: { chatResponses in
            let chatSchemas = chatResponses.flatMap({$0.chatSchema})
            self.saveChats(chatSchemas)
            self.dataRefresher.refreshChats()
        }, onError: { error in
            self.saveChatsError(error)
        })
    }
}

// MARK: Fetch chat messages
extension ChatController {
    func fetchChatMessages(chatId: String) {
        self.loadChatMessages(chatId: chatId)
        let params: NetworkParameters = [
            .query : [
                .limit(DefaultChatMessagesLimit)
            ],
            .path : [
                .chatId(chatId)
            ]
        ]
        self.dataRefresher.assignRefreshChatMessagesCall(self.fetchChatMessages, chatId: chatId)
        self.callNetwork(self.networkController.fetchChatMessages, params: params, onSuccess: { chatMessageResponses in
            let messageChatSchemas = chatMessageResponses.flatMap({$0.messageChatSchema})
            self.saveChatMessages(messageChatSchemas)
            self.dataRefresher.refreshChatMessages()
        }, onError: { error in
            self.saveChatMessagesError(error)
        })
    }
}

// MARK: Send chat message
extension ChatController {
    func sendChatMessage(chatId: String, messageText: String) {
        let params: NetworkParameters = [
            .path : [
                .chatId(chatId)
            ],
            .body : [
                .messageText(messageText)
            ]
        ]
        self.callNetwork(self.networkController.sendChatMessage, params: params, onSuccess: { chatMessageResponse in
            if let messageChatSchema = chatMessageResponse.messageChatSchema {
                self.saveChatMessageSent(messageChatSchema)
            }
        }, onError: { error in
            self.saveChatMessageSentError(error)
        })
    }
}

// MARK: Stop refresh chats
extension ChatController {
    func stopRefreshChats() {
        self.dataRefresher.stopRefreshChats()
    }
}

// MARK: Stop refresh chat messages
extension ChatController {
    func stopRefreshChatMessages() {
        self.dataRefresher.stopRefreshChatMessages()
    }
}

// MARK: Discard chat messages
extension ChatController {
    func discardChatMessages() {
        self.modelController.repository.chatRepository.unloadChatMessages()
    }
}

// MARK: Private methods
extension ChatController {
    private func saveChats(_ chatSchemas: [ChatSchema]) {
        self.modelController.repository.chatRepository.saveChats(chatSchemas)
    }
    
    private func saveChatsError(_ error: Error) {
        self.modelController.repository.chatRepository.saveChatsError(error)
    }

    private func loadChatMessages(chatId: String) {
        self.modelController.repository.chatRepository.loadChatMessages(chatId: chatId)
    }
    
    private func saveChatMessages(_ messageChatSchemas: [MessageChatSchema]) {
        self.modelController.repository.chatRepository.saveChatMessages(messageChatSchemas)
    }

    private func saveChatMessagesError(_ error: Error) {
        self.modelController.repository.chatRepository.saveChatMessagesError(error)
    }
    
    private func saveChatMessageSent(_ messageChatSchema: MessageChatSchema) {
        self.modelController.repository.chatRepository.saveChatMessageSent(messageChatSchema)
    }
    
    private func saveChatMessageSentError(_ error: Error) {
        self.modelController.repository.chatRepository.saveChatMessageSentError(error)
    }
}
