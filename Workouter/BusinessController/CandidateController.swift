//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol CandidateControllerProtocol: class {
    func fetchCandidates()
    func fetchCandidateProfile(candidateId: String)
    func evaluate(candidateId: String, evaluation: EvaluationValue)
    func discardCandidateProfile()
}

final class CandidateController: ControllerType, CandidateControllerProtocol {
    private(set) var tokenRefresher: TokenRefresher
    private let modelController: ModelController
    private let networkController: NetworkController
    
    init(modelController: ModelController, networkController: NetworkController) {
        self.modelController = modelController
        self.networkController = networkController
        self.tokenRefresher = TokenRefresher(modelController: self.modelController, networkController: self.networkController)
    }
}

// MARK: Fetch candidates
extension CandidateController {
    func fetchCandidates() {
        let params: NetworkParameters = [
            .query : [
                .limit(DefaultDiscoverLimit)
            ]
        ]
        self.callNetwork(self.networkController.fetchCandidates, params: params, onSuccess: { candidateResponses in
            let candidateSchemas = candidateResponses.flatMap({$0.candidateSchema})
            self.saveCandidates(candidateSchemas)
        }, onError: { error in
            self.saveCandidatesError(error)
        })
    }
}

// MARK: Fetch candidate profile
extension CandidateController {
    func fetchCandidateProfile(candidateId: String) {
        let params: NetworkParameters = [
            .path : [
                .candidateId(candidateId)
            ]
        ]
        self.callNetwork(self.networkController.fetchCandidateProfile, params: params, onSuccess: { profileResponse in
            if let profileSchema = profileResponse.profileSchema {
                self.saveCandidateProfile(profileSchema)
            }
        }, onError: { error in
            self.saveCandidateProfileError(error)
        })
    }
}

// MARK: Evaluate
extension CandidateController {
    func evaluate(candidateId: String, evaluation: EvaluationValue) {
        let params: NetworkParameters = [
            .body : [
                .candidateId(candidateId),
                .evaluationValue(evaluation.rawValue)
            ]
        ]
        self.callNetwork(self.networkController.evaluate, params: params, onSuccess: { evaluationResponse in
            if let evaluationSchema = evaluationResponse.evaluationSchema {
                self.saveEvaluation(evaluationSchema)
            }
        }, onError: { error in
            self.saveEvaluationError(error)
        })
    }
}

// MARK: Discard candidate profile
extension CandidateController {
    func discardCandidateProfile() {
        self.modelController.repository.candidateRepository.unloadCandidateProfile()
    }
}

// MARK: Private methods
extension CandidateController {
    private func saveCandidates(_ candidateSchemas: [CandidateSchema]) {
        self.modelController.repository.candidateRepository.saveCandidates(candidateSchemas)
    }
    
    private func saveCandidatesError(_ error: Error) {
        self.modelController.repository.candidateRepository.saveCandidatesError(error)
    }

    private func saveCandidateProfile(_ profileSchema: ProfileSchema) {
        self.modelController.repository.candidateRepository.saveCandidateProfile(profileSchema)
    }

    private func saveCandidateProfileError(_ error: Error) {
        self.modelController.repository.candidateRepository.saveCandidateProfileError(error)
    }
    
    private func saveEvaluation(_ evaluationSchema: EvaluationSchema) {
        self.modelController.repository.candidateRepository.saveEvaluation(evaluationSchema)
    }
    
    private func saveEvaluationError(_ error: Error) {
        self.modelController.repository.candidateRepository.saveEvaluationError(error)
    }
}

