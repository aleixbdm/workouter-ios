//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class CandidateCard: UIView {
    
    @IBOutlet var contentView: CandidateCard!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var city: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder unarchiver: NSCoder) {
        super.init(coder: unarchiver)
        self.setupView()
    }
    
    private func setupView() {
        let nib: UINib = UINib(nibName: String(describing: CandidateCard.self), bundle: Bundle.main)
        nib.instantiate(withOwner: self, options: nil)
        self.addSubview(self.contentView)
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let bindings: [String: Any] = ["view": self.contentView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options:[], metrics:nil, views: bindings))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options:[], metrics:nil, views: bindings))
        
    }
    
    public func setCandidate(_ candidate: Candidate) {
        if let imageUrl = candidate.imageUrls.first {
            self.photo.setImage(url: imageUrl)
        }
        self.name.text = candidate.name
        self.city.text = candidate.city
    }
}
