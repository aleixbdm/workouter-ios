//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class MDSlider: UISlider {
    func sliderChanged() {
        self.setValue(Float(lroundf(self.value)), animated: true)
    }
}
