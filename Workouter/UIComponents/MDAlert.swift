//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class MDAlert: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.updateBackground()
    }
    
    func updateBackground() {
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.layer.borderWidth = 3
        self.layer.borderColor = UIColor.primary().cgColor
        
        self.backgroundColor = UIColor.white
    }
    
}
