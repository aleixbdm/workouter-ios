//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class MDRoundView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
            self.layer.masksToBounds = self.cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            self.layer.borderColor = self.borderColor?.cgColor
        }
    }
    
    @IBInspectable var roundViewBackgroundColor: UIColor? {
        didSet {
            self.backgroundColor = self.roundViewBackgroundColor
        }
    }
}
