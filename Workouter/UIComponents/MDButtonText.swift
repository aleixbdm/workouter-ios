//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class MDButtonText: MDButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.updateBackground()
    }
    
    func updateBackground() {
        self.backgroundColor = UIColor.backgroundButtonColor()
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
    }
    
}
