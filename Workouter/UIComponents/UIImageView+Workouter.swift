//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

extension UIImageView {
    func setImage(url: String, rounded: Bool = false) {
        if let url = URL(string: url) {
            self.af_setImage(withURL: url) { Void in
                if rounded {
                    self.image = self.image?.af_imageRoundedIntoCircle()
                }
            }
        }
    }
}
