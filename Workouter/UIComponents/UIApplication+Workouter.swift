//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return self.topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return self.topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return self.topViewController(presented)
        }
        return base
    }
    
    func restartApp(animated: Bool) {
        self.rootNavigationController?.popToRootViewController(animated: false)
        self.rootViewController?.dismiss(animated: animated, completion: nil)
    }
    
    private var rootNavigationController: UINavigationController? {
        return UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
    }
    
    private var rootViewController: UIViewController? {
        return self.rootNavigationController?.viewControllers.first
    }
}
