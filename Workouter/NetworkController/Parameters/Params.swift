//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

enum ParamKeys {
    case query
    case path
    case body
}

enum ParamValues {
    case email(String)
    case password(String)
    case accessToken(String)
    case userId(String)
    case name(String)
    case firstName(String)
    case imageUrls([String])
    case age(Int)
    case gender(String)
    case preference(String)
    case city(String)
    case candidateId(String)
    case evaluationValue(String)
    case chatId(String)
    case messageText(String)
    case limit(Int)
    
    var key: String {
        let mirror = Mirror(reflecting: self)
        if let label = mirror.children.first?.label {
            return label
        } else {
            return String(describing: self)
        }
    }
    
    var associatedValue: Any {
        let mirror = Mirror(reflecting: self)
        if let value = mirror.children.first?.value {
            return value
        } else {
            return String(describing: self)
        }
    }
}
