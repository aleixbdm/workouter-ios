//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

struct NetworkParameters {
    typealias DictionaryType = [ParamKeys : [ParamValues]]
    private var params = DictionaryType()
    init(params: DictionaryType) {
        self.params = params
    }
}

extension NetworkParameters: Collection {
    typealias Index = DictionaryType.Index
    typealias Element = DictionaryType.Element
    var startIndex: Index { return self.params.startIndex }
    var endIndex: Index { return self.params.endIndex }
    subscript(index: Index) -> Iterator.Element {
        get { return self.params[index] }
    }
    func index(after i: NetworkParameters.DictionaryType.Index) -> NetworkParameters.DictionaryType.Index {
        return self.params.index(after: i)
    }
}

extension NetworkParameters {
    subscript(paramsKey: ParamKeys) -> [ParamValues] {
        get { return self.params[paramsKey] ?? [] }
        set { self.params[paramsKey] = newValue }
    }
    
    subscript<T>(paramsKey: ParamKeys, paramsValue: ParamValues) -> T? {
        let value = params[paramsKey]?.filter({$0.key == paramsValue.key}).first
        return value?.associatedValue as? T
    }
}

extension NetworkParameters: ExpressibleByDictionaryLiteral {
    typealias Key = ParamKeys
    typealias Value = [ParamValues]
    init(dictionaryLiteral elements: (Key, Value)...) {
        for (paramsKey, value) in elements {
            self.params[paramsKey] = value
        }
    }
}
