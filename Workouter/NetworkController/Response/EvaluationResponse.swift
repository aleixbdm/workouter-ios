//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class EvaluationResponse: BaseResponse {
    var evaluationSchema: EvaluationSchema?

    public struct Keys {
        static let Evaluation = "evaluation"
    }

    required public init(map: Map) throws {
        try super.init(map: map)
        self.evaluationSchema = try? map.value(Keys.Evaluation)
    }

    override public func mapping(map: Map) {
        super.mapping(map: map)
        self.evaluationSchema >>> map[Keys.Evaluation]
    }
}

