//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class AuthResponse: BaseResponse {
    var authSchema: AuthSchema?
    
    public struct Keys {
        static let Auth = "auth"
    }
    
    required public init(map: Map) throws {
        try super.init(map: map)
        self.authSchema = try? map.value(Keys.Auth)
    }
    
    override public func mapping(map: Map) {
        super.mapping(map: map)
        self.authSchema >>> map[Keys.Auth]
    }
}
