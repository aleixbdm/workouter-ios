//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class CandidateResponse: BaseResponse {
    var candidateSchema: CandidateSchema?
    
    public struct Keys {
        static let Candidate = "candidate"
    }
    
    required public init(map: Map) throws {
        try super.init(map: map)
        self.candidateSchema = try? map.value(Keys.Candidate)
    }
    
    override public func mapping(map: Map) {
        super.mapping(map: map)
        self.candidateSchema >>> map[Keys.Candidate]
    }
}

