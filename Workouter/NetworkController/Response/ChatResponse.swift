//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class ChatResponse: BaseResponse {
    var chatSchema: ChatSchema?
    
    public struct Keys {
        static let Chat = "chat"
    }
    
    required public init(map: Map) throws {
        try super.init(map: map)
        self.chatSchema = try? map.value(Keys.Chat)
    }
    
    override public func mapping(map: Map) {
        super.mapping(map: map)
        self.chatSchema >>> map[Keys.Chat]
    }
}


