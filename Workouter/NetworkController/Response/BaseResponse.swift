//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class BaseResponse: ImmutableMappable {
    var error: ErrorSchema?
    var authError: ErrorSchema?
    
    public struct Keys {
        static let Error = "error"
        static let AuthError = "auth_error"
    }
    
    public required init(map: Map) throws {
        self.error = try? map.value(Keys.Error)
        self.authError = try? map.value(Keys.AuthError)
    }
    
    public func mapping(map: Map) {
        self.error >>> map[Keys.Error]
        self.authError >>> map[Keys.AuthError]
    }
}
