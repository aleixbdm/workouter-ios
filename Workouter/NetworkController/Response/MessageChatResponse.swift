//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class MessageChatResponse: BaseResponse {
    var messageChatSchema: MessageChatSchema?

    public struct Keys {
        static let MessageChat = "message_chat"
    }

    required public init(map: Map) throws {
        try super.init(map: map)
        self.messageChatSchema = try? map.value(Keys.MessageChat)
    }

    override public func mapping(map: Map) {
        super.mapping(map: map)
        self.messageChatSchema >>> map[Keys.MessageChat]
    }
}
