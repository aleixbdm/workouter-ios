//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class ProfileResponse: BaseResponse {
    var profileSchema: ProfileSchema?
    
    public struct Keys {
        static let User = "user"
    }
    
    required public init(map: Map) throws {
        try super.init(map: map)
        self.profileSchema = try? map.value(Keys.User)
    }
    
    override public func mapping(map: Map) {
        super.mapping(map: map)
        self.profileSchema >>> map[Keys.User]
    }
}
