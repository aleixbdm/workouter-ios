//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class MessageChatSchema: ImmutableMappable {
    var id: String
    var chatId: String
    var senderUserId: String
    var messageText: String
    var createdAt: Double
    
    public struct Keys {
        static let Id = "id"
        static let ChatId = "chat_id"
        static let SenderUserId = "sender_user_id"
        static let MessageText = "message_text"
        static let CreatedAt = "created_at"
    }
    
    public required init(map: Map) throws {
        self.id = try map.value(Keys.Id)
        self.chatId = try map.value(Keys.ChatId)
        self.senderUserId = try map.value(Keys.SenderUserId)
        self.messageText = try map.value(Keys.MessageText)
        self.createdAt = try map.value(Keys.CreatedAt)
    }
    
    public func mapping(map: Map) {
        self.id >>> map[Keys.Id]
        self.chatId >>> map[Keys.ChatId]
        self.senderUserId >>> map[Keys.SenderUserId]
        self.messageText >>> map[Keys.MessageText]
        self.createdAt >>> map[Keys.CreatedAt]
    }
}
