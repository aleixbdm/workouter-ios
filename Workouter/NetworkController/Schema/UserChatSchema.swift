//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class UserChatSchema: ImmutableMappable {
    var id: String
    var name: String
    var firstName: String
    var imageUrl: String
    
    public struct Keys {
        static let Id = "id"
        static let Name = "name"
        static let FirstName = "first_name"
        static let ImageUrl = "image_url"
    }
    
    public required init(map: Map) throws {
        self.id = try map.value(Keys.Id)
        self.name = try map.value(Keys.Name)
        self.firstName = try map.value(Keys.FirstName)
        self.imageUrl = try map.value(Keys.ImageUrl)
    }
    
    public func mapping(map: Map) {
        self.id >>> map[Keys.Id]
        self.name >>> map[Keys.Name]
        self.firstName >>> map[Keys.FirstName]
        self.imageUrl >>> map[Keys.ImageUrl]
    }
}

