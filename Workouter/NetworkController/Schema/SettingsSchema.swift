//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class SettingsSchema: ImmutableMappable {
    var visible: Bool?
    var yearRangeMax: Int?
    var yearRangeMin: Int?
    var searchRange: Int?
    var city: String?
    var preference: String?
    
    public struct Keys {
        static let Visible = "visible"
        static let YearRangeMax = "year_range_max"
        static let YearRangeMin = "year_range_min"
        static let SearchRange = "search_range"
        static let City = "city"
        static let Preference = "preference"
    }
    
    public init(settings: Settings) {
        self.visible = settings.visible.value
        self.yearRangeMax = settings.yearRangeMax.value
        self.yearRangeMin = settings.yearRangeMin.value
        self.searchRange = settings.yearRangeMin.value
        self.city = settings.city
        self.preference = settings.preferenceType.rawValue
    }
    
    public required init(map: Map) throws {
        self.visible = try? map.value(Keys.Visible)
        self.yearRangeMax = try? map.value(Keys.YearRangeMax)
        self.yearRangeMin = try? map.value(Keys.YearRangeMin)
        self.searchRange = try? map.value(Keys.SearchRange)
        self.city = try? map.value(Keys.City)
        self.preference = try? map.value(Keys.Preference)
    }
    
    public func mapping(map: Map) {
        self.visible >>> map[Keys.Visible]
        self.yearRangeMax >>> map[Keys.YearRangeMax]
        self.yearRangeMin >>> map[Keys.YearRangeMin]
        self.searchRange >>> map[Keys.SearchRange]
        self.city >>> map[Keys.City]
        self.preference >>> map[Keys.Preference]
    }
}
