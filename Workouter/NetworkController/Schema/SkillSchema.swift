//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class SkillSchema: ImmutableMappable {
    var category: String
    var value: Int
    
    public struct Keys {
        static let Category = "category"
        static let Value = "value"
    }
    
    public init(skill: Skill) {
        self.category = skill.categoryType.rawValue
        self.value = skill.value.value ?? 0
    }
    
    public required init(map: Map) throws {
        self.category = try map.value(Keys.Category)
        self.value = try map.value(Keys.Value)
    }
    
    public func mapping(map: Map) {
        self.category >>> map[Keys.Category]
        self.value >>> map[Keys.Value]
    }
}

