//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class CandidateSchema: ImmutableMappable {
    var id: String
    var name: String
    var imageUrls: [String]
    var age: Int
    var skills: [SkillSchema]?
    var city: String
    
    public struct Keys {
        static let Id = "id"
        static let Name = "name"
        static let ImageUrls = "image_urls"
        static let Age = "age"
        static let Skills = "skills"
        static let City = "city"
    }
    
    public required init(map: Map) throws {
        self.id = try map.value(Keys.Id)
        self.name = try map.value(Keys.Name)
        self.imageUrls = try map.value(Keys.ImageUrls)
        self.age = try map.value(Keys.Age)
        self.skills = try? map.value(Keys.Skills)
        self.city = try map.value(Keys.City)
    }
    
    public func mapping(map: Map) {
        self.id >>> map[Keys.Id]
        self.name >>> map[Keys.Name]
        self.imageUrls >>> map[Keys.ImageUrls]
        self.age >>> map[Keys.Age]
        self.skills >>> map[Keys.Skills]
        self.city >>> map[Keys.City]
    }
}
