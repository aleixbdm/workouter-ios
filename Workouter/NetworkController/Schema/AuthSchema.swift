//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class AuthSchema: ImmutableMappable {
    var accessToken: String
    var userId: String
    
    public struct Keys {
        static let AccessToken = "access_token"
        static let UserId = "user_id"
    }
    
    public init(auth: Auth) {
        self.accessToken = auth.accessToken
        self.userId = auth.userId
    }
    
    public required init(map: Map) throws {
        self.accessToken = try map.value(Keys.AccessToken)
        self.userId = try map.value(Keys.UserId)
    }
    
    public func mapping(map: Map) {
        self.accessToken >>> map[Keys.AccessToken]
        self.userId >>> map[Keys.UserId]
    }
}
