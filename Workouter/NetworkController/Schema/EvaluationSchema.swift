//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class EvaluationSchema: ImmutableMappable {
    var id: String
    var senderUserId: String
    var receiverUserId: String
    var value: String
    var chatId: String?

    public struct Keys {
        static let Id = "id"
        static let SenderUserId = "sender_user_id"
        static let ReceiverUserId = "receiver_user_id"
        static let Value = "evaluation_value"
        static let ChatId = "chat_id"
    }

    public required init(map: Map) throws {
        self.id = try map.value(Keys.Id)
        self.senderUserId = try map.value(Keys.SenderUserId)
        self.receiverUserId = try map.value(Keys.ReceiverUserId)
        self.value = try map.value(Keys.Value)
        self.chatId = try? map.value(Keys.ChatId)
    }

    public func mapping(map: Map) {
        self.id >>> map[Keys.Id]
        self.senderUserId >>> map[Keys.SenderUserId]
        self.receiverUserId >>> map[Keys.ReceiverUserId]
        self.value >>> map[Keys.Value]
        self.chatId >>> map[Keys.ChatId]
    }
}

