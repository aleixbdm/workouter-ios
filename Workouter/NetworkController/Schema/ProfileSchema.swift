//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class ProfileSchema: ImmutableMappable {
    var id: String
    var name: String?
    var firstName: String?
    var imageUrls: [String]?
    var age: Int?
    var gender: String?
    var generalInformation: String?
    var currentJob: String?
    var studies: String?
    var favouriteSong: String?
    var skills: [SkillSchema]?
    var settings: SettingsSchema?
    
    public struct Keys {
        static let Id = "id"
        static let Name = "name"
        static let FirstName = "first_name"
        static let ImageUrls = "image_urls"
        static let Age = "age"
        static let Gender = "gender"
        static let GeneralInformation = "description"
        static let CurrentJob = "current_job"
        static let Studies = "studies"
        static let FavouriteSong = "favourite_song"
        static let Skills = "skills"
        static let Settings = "settings"
    }
    
    public init?(profile: Profile) {
        guard let id = profile.id else {
            return nil
        }
        self.id = id
        self.name = profile.name
        self.firstName = profile.firstName
        self.imageUrls = profile.imageUrls.map({$0})
        self.age = profile.age.value
        self.gender = profile.genderType.rawValue
        self.generalInformation = profile.generalInformation
        self.currentJob = profile.currentJob
        self.studies = profile.studies
        self.favouriteSong = profile.favouriteSong
        self.skills = profile.skills.map({SkillSchema(skill: $0)})
        if let settings = profile.settings {
            self.settings = SettingsSchema(settings: settings)
        }
    }
    
    public required init(map: Map) throws {
        self.id = try map.value(Keys.Id)
        self.name = try? map.value(Keys.Name)
        self.firstName = try? map.value(Keys.FirstName)
        self.imageUrls = try? map.value(Keys.ImageUrls)
        self.age = try? map.value(Keys.Age)
        self.gender = try? map.value(Keys.Gender)
        self.generalInformation = try? map.value(Keys.GeneralInformation)
        self.currentJob = try? map.value(Keys.CurrentJob)
        self.studies = try? map.value(Keys.Studies)
        self.favouriteSong = try? map.value(Keys.FavouriteSong)
        self.skills = try? map.value(Keys.Skills)
        self.settings = try? map.value(Keys.Settings)
    }
    
    public func mapping(map: Map) {
        self.id >>> map[Keys.Id]
        self.name >>> map[Keys.Name]
        self.firstName >>> map[Keys.FirstName]
        self.imageUrls >>> map[Keys.ImageUrls]
        self.age >>> map[Keys.Age]
        self.gender >>> map[Keys.Gender]
        self.generalInformation >>> map[Keys.GeneralInformation]
        self.currentJob >>> map[Keys.CurrentJob]
        self.studies >>> map[Keys.Studies]
        self.favouriteSong >>> map[Keys.FavouriteSong]
        self.skills >>> map[Keys.Skills]
        self.settings >>> map[Keys.Settings]
    }
}
