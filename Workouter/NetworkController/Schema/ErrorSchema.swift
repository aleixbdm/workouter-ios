//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class ErrorSchema: ImmutableMappable {
    var message: String?
    
    public struct Keys {
        static let Message = "message"
    }
    
    public required init(map: Map) throws {
        self.message = try map.value(Keys.Message)
    }
    
    public func mapping(map: Map) {
        self.message >>> map[Keys.Message]
    }
}

