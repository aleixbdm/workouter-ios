//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

class ChatSchema: ImmutableMappable {
    var id: String
    var candidateUser: UserChatSchema
    var lastMessage: MessageChatSchema?
    var updatedAt: Double
    
    public struct Keys {
        static let Id = "id"
        static let CandidateUser = "candidate_user.user_chat"
        static let LastMessage = "last_message.message_chat"
        static let UpdatedAt = "updated_at"
    }
    
    public required init(map: Map) throws {
        self.id = try map.value(Keys.Id)
        self.candidateUser = try map.value(Keys.CandidateUser)
        self.lastMessage = try? map.value(Keys.LastMessage)
        self.updatedAt = try map.value(Keys.UpdatedAt)
    }
    
    public func mapping(map: Map) {
        self.id >>> map[Keys.Id]
        self.candidateUser >>> map[Keys.CandidateUser]
        self.lastMessage >>> map[Keys.LastMessage]
        self.updatedAt >>> map[Keys.UpdatedAt]
    }
}

