//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol NetworkControllerType {
    func formatUrl(path: String, networkParameters: NetworkParameters?) -> URL
}
extension NetworkControllerType {
    func formatUrl(path: String, networkParameters: NetworkParameters?) -> URL {
        if let url = URL(string: BaseUrl), let formatedUrl = url.appendingPathComponent(path).with(networkParameters: networkParameters) {
            return formatedUrl
        } else {
            fatalError()
        }
    }
}
