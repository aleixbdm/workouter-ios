//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol ChatRestAPIProtocol: class {
    func fetchChats(params: NetworkParameters?, completion: @escaping ([ChatResponse]?, Error?) -> ())
    func fetchChatMessages(params: NetworkParameters?, completion: @escaping ([MessageChatResponse]?, Error?) -> ())
    func sendChatMessage(params: NetworkParameters?, completion: @escaping (MessageChatResponse?, Error?) -> ())
}

final class ChatRestAPI: ApiType, ChatRestAPIProtocol {
    private(set) var url: URL
    
    init(url: URL) {
        self.url = url
    }
}

// MARK: Fetch chats
extension ChatRestAPI {
    func fetchChats(params _: NetworkParameters?, completion: @escaping ([ChatResponse]?, Error?) -> ()){
        self.call(method: .get, completion: completion)
    }
}

// MARK: Fetch chat messages
extension ChatRestAPI {
    func fetchChatMessages(params _: NetworkParameters?, completion: @escaping ([MessageChatResponse]?, Error?) -> ()){
        self.call(method: .get, completion: completion)
    }
}

// MARK: Send chat message
extension ChatRestAPI {
    func sendChatMessage(params: NetworkParameters?, completion: @escaping (MessageChatResponse?, Error?) -> ()){
        let parameters: SendMessageRequest? = SendMessageRequest(builder: SendMessageRequestBuilder {
            $0.messageText = params?[.body, .messageText(DefaultString)]
        })
        self.call(method: .post, parameters: parameters?.toJSON(), completion: completion)
    }
}
