//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol CandidateRestAPIProtocol: class {
    func fetchCandidates(params: NetworkParameters?, completion: @escaping ([CandidateResponse]?, Error?) -> ())
    func fetchCandidateProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ())
    func evaluate(params: NetworkParameters?, completion: @escaping (EvaluationResponse?, Error?) -> ())
}

final class CandidateRestAPI: ApiType, CandidateRestAPIProtocol {
    private(set) var url: URL
    
    init(url: URL) {
        self.url = url
    }
}

// MARK: Fetch candidates
extension CandidateRestAPI {
    func fetchCandidates(params _: NetworkParameters?, completion: @escaping ([CandidateResponse]?, Error?) -> ()){
        self.call(method: .get, completion: completion)
    }
}

// MARK: Fetch candidate profile
extension CandidateRestAPI {
    func fetchCandidateProfile(params _: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()){
        self.call(method: .get, completion: completion)
    }
}

// MARK: Evaluate
extension CandidateRestAPI {
    func evaluate(params: NetworkParameters?, completion: @escaping (EvaluationResponse?, Error?) -> ()){
        let parameters: EvaluationRequest? = EvaluationRequest(builder: EvaluationRequestBuilder {
            $0.value = params?[.body, .evaluationValue(DefaultString)]
            $0.candidateId = params?[.body, .candidateId(DefaultString)]
        })
        self.call(method: .post, parameters: parameters?.toJSON(), completion: completion)
    }
}
