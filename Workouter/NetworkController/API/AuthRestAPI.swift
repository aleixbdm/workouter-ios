//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol AuthRestAPIProtocol: class {
    func login(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ())
    func loginWithFB(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ())
    func register(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ())
    func refreshToken(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ())
}

final class AuthRestAPI: ApiType, AuthRestAPIProtocol {
    private(set) var url: URL

    init(url: URL) {
        self.url = url
    }
}

// MARK: Login
extension AuthRestAPI {
    func login(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let parameters: AuthRequest? = AuthRequest(builder: AuthRequestBuilder {
            $0.email = params?[.body, .email(DefaultString)]
            $0.password = params?[.body, .password(DefaultString)]
        })
        self.call(method: .post, parameters: parameters?.toJSON(), completion: completion)
    }
}

// MARK: Login with fb
extension AuthRestAPI {
    func loginWithFB(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let parameters: FacebookAuthRequest? = FacebookAuthRequest(builder: FacebookAuthRequestBuilder {
            $0.accessToken = params?[.body, .accessToken(DefaultString)]
        })
        self.call(method: .post, parameters: parameters?.toJSON(), completion: completion)
    }
}

// MARK: Register
extension AuthRestAPI {
    func register(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let parameters: AuthRequest? = AuthRequest(builder: AuthRequestBuilder {
            $0.email = params?[.body, .email(DefaultString)]
            $0.password = params?[.body, .password(DefaultString)]
        })
        self.call(method: .post, parameters: parameters?.toJSON(), completion: completion)
    }
}

// MARK: Refresh token
extension AuthRestAPI {
    func refreshToken(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let parameters: RefreshTokenRequest? = RefreshTokenRequest(builder: RefreshTokenRequestBuilder {
            $0.userId = params?[.body, .userId(DefaultString)]
        })
        self.call(method: .post, parameters: parameters?.toJSON(), completion: completion)
    }
}
