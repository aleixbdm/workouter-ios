//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

protocol ApiType {
    var url: URL { get }
    init(url: URL)
}
extension ApiType {
    func call<T: BaseResponse>(method: HTTPMethod, parameters: Parameters? = nil, completion: @escaping (T?, Error?) -> ()) {
        Logger.log("call url: \(self.url)")
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.adapter = AuthAdapter()
        sessionManager.request(self.url, method: method, parameters: parameters, encoding: JSONEncoding.default)
            .responseObject { (response: DataResponse<T>) in
                switch response.result {
                case .success(let response):
                    if let error = response.error {
                        completion(nil, MeetDayError.apiError(message: error.message))
                    } else if let authError = response.authError {
                        completion(nil, MeetDayError.authError(message: authError.message))
                    } else {
                        completion(response, nil)
                    }
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    func call<T: BaseResponse>(method: HTTPMethod, parameters: Parameters? = nil, completion: @escaping ([T]?, Error?) -> ()) {
        Logger.log("call url: \(self.url)")
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.adapter = AuthAdapter()
        sessionManager.request(self.url, method: method, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { (jsonResponse) in
                switch jsonResponse.result {
                case .success(let json):
                    if jsonResponse.response?.statusCode != 200, let response = try? Mapper<T>().map(JSON: json as? [String:Any] ?? [:]) {
                        if let error = response.error {
                            completion(nil, MeetDayError.apiError(message: error.message))
                        } else if let authError = response.authError {
                            completion(nil, MeetDayError.authError(message: authError.message))
                        } else {
                            // Should not get here
                            completion(nil, nil)
                        }
                    } else if let responses = try? Mapper<T>().mapArray(JSONArray: json as? [[String : Any]] ?? []) {
                            completion(responses, nil)
                    } else {
                        // Should not get here
                        completion(nil, nil)
                    }
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
}
