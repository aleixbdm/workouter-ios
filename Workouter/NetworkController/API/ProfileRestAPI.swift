//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol ProfileRestAPIProtocol: class {
    func fetchProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ())
    func createProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ())
}

final class ProfileRestAPI: ApiType, ProfileRestAPIProtocol {
    private(set) var url: URL

    init(url: URL) {
        self.url = url
    }
}

// MARK: Fetch Profile
extension ProfileRestAPI {
    func fetchProfile(params _: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()){
        self.call(method: .get, completion: completion)
    }
}

// MARK: Create Profile
extension ProfileRestAPI {
    func createProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()){
        let parameters: CreateProfileRequest? = CreateProfileRequest(builder: CreateProfileRequestBuilder {
            $0.name = params?[.body, .name(DefaultString)]
            $0.firstName = params?[.body, .firstName(DefaultString)]
            $0.imageUrls = params?[.body, .imageUrls(DefaultArrayString)]
            $0.age = params?[.body, .age(DefaultInt)]
            $0.gender = params?[.body, .gender(DefaultString)]
            $0.preference = params?[.body, .preference(DefaultString)]
            $0.city = params?[.body, .city(DefaultString)]
        })
        self.call(method: .put, parameters: parameters?.toJSON(), completion: completion)
    }
}
