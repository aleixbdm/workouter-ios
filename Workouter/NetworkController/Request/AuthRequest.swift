//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

public class AuthRequestBuilder {
    public var email: String?
    public var password: String?
    
    public typealias BuilderClosure = (AuthRequestBuilder) -> ()
    
    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct AuthRequest {
    public let email: String
    public let password: String
    
    public init?(builder: AuthRequestBuilder) {
        guard let email = builder.email,
            let password = builder.password else {
            return nil
        }
        self.email = email
        self.password = password
    }
}

extension AuthRequest: ImmutableMappable {
    public struct Keys {
        static let Email = "email"
        static let Password = "password"
    }
    
    public init(map: Map) throws {
        self.email = try map.value(Keys.Email)
        self.password = try map.value(Keys.Password)
    }
    
    public func mapping(map: Map) {
        self.email >>> map[Keys.Email]
        self.password >>> map[Keys.Password]
    }
}
