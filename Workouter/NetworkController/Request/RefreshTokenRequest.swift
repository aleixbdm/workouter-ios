//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

public class RefreshTokenRequestBuilder {
    public var userId: String?
    
    public typealias BuilderClosure = (RefreshTokenRequestBuilder) -> ()
    
    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct RefreshTokenRequest {
    public let userId: String
    
    public init?(builder: RefreshTokenRequestBuilder) {
        guard let userId = builder.userId else {
                return nil
        }
        self.userId = userId
    }
}

extension RefreshTokenRequest: ImmutableMappable {
    public struct Keys {
        static let UserId = "user_id"
    }
    
    public init(map: Map) throws {
        self.userId = try map.value(Keys.UserId)
    }
    
    public func mapping(map: Map) {
        self.userId >>> map[Keys.UserId]
    }
}

