//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

public class CreateProfileRequestBuilder {
    public var name: String?
    public var firstName: String?
    public var imageUrls: [String]?
    public var age: Int?
    public var gender: String?
    public var preference: String?
    public var city: String?
    
    public typealias BuilderClosure = (CreateProfileRequestBuilder) -> ()
    
    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct CreateProfileRequest {
    public let name: String
    public let firstName: String
    public let imageUrls: [String]
    public let age: Int
    public let gender: String
    public let preference: String
    public let city: String
    
    public init?(builder: CreateProfileRequestBuilder) {
        guard let name = builder.name,
            let firstName = builder.firstName,
            let imageUrls = builder.imageUrls,
            let age = builder.age,
            let gender = builder.gender,
            let preference = builder.preference,
            let city = builder.city else {
                return nil
        }
        self.name = name
        self.firstName = firstName
        self.imageUrls = imageUrls
        self.age = age
        self.gender = gender
        self.preference = preference
        self.city = city
    }
}

extension CreateProfileRequest: ImmutableMappable {
    public struct Keys {
        static let Name = "user_basic_profile.name"
        static let FirstName = "user_basic_profile.first_name"
        static let ImageUrls = "user_basic_profile.image_urls"
        static let Age = "user_basic_profile.age"
        static let Gender = "user_basic_profile.gender"
        static let Preference = "user_basic_profile.preference"
        static let City = "user_basic_profile.city"
    }
    
    public init(map: Map) throws {
        self.name = try map.value(Keys.Name)
        self.firstName = try map.value(Keys.FirstName)
        self.imageUrls = try map.value(Keys.ImageUrls)
        self.age = try map.value(Keys.Age)
        self.gender = try map.value(Keys.Gender)
        self.preference = try map.value(Keys.Preference)
        self.city = try map.value(Keys.City)
    }
    
    public func mapping(map: Map) {
        self.name >>> map[Keys.Name]
        self.firstName >>> map[Keys.FirstName]
        self.imageUrls >>> map[Keys.ImageUrls]
        self.age >>> map[Keys.Age]
        self.gender >>> map[Keys.Gender]
        self.preference >>> map[Keys.Preference]
        self.city >>> map[Keys.City]
    }
}
