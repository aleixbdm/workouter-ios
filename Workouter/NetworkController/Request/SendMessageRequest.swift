//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

public class SendMessageRequestBuilder {
    public var messageText: String?
    
    public typealias BuilderClosure = (SendMessageRequestBuilder) -> ()
    
    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct SendMessageRequest {
    public let messageText: String
    
    public init?(builder: SendMessageRequestBuilder) {
        guard let messageText = builder.messageText else {
                return nil
        }
        self.messageText = messageText
    }
}

extension SendMessageRequest: ImmutableMappable {
    public struct Keys {
        static let MessageText = "message_text"
    }
    
    public init(map: Map) throws {
        self.messageText = try map.value(Keys.MessageText)
    }
    
    public func mapping(map: Map) {
        self.messageText >>> map[Keys.MessageText]
    }
}
