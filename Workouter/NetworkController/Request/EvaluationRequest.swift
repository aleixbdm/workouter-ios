//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

public class EvaluationRequestBuilder {
    public var value: String?
    public var candidateId: String?

    public typealias BuilderClosure = (EvaluationRequestBuilder) -> ()

    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct EvaluationRequest {
    public let value: String
    public let candidateId: String

    public init?(builder: EvaluationRequestBuilder) {
        guard let value = builder.value,
            let candidateId = builder.candidateId else {
                return nil
        }
        self.value = value
        self.candidateId = candidateId
    }
}

extension EvaluationRequest: ImmutableMappable {
    public struct Keys {
        static let Value = "evaluation_value"
        static let CandidateId = "candidate_id"
    }

    public init(map: Map) throws {
        self.value = try map.value(Keys.Value)
        self.candidateId = try map.value(Keys.CandidateId)
    }

    public func mapping(map: Map) {
        self.value >>> map[Keys.Value]
        self.candidateId >>> map[Keys.CandidateId]
    }
}

