//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper

public class FacebookAuthRequestBuilder {
    public var accessToken: String?

    public typealias BuilderClosure = (FacebookAuthRequestBuilder) -> ()

    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct FacebookAuthRequest {
    public let accessToken: String

    public init?(builder: FacebookAuthRequestBuilder) {
        guard let accessToken = builder.accessToken else {
                return nil
        }
        self.accessToken = accessToken
    }
}

extension FacebookAuthRequest: ImmutableMappable {
    public struct Keys {
        static let AccessToken = "access_token"
    }

    public init(map: Map) throws {
        self.accessToken = try map.value(Keys.AccessToken)
    }

    public func mapping(map: Map) {
        self.accessToken >>> map[Keys.AccessToken]
    }
}
