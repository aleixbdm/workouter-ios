//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Alamofire
import AlamofireObjectMapper

class NetworkController: NetworkControllerType, AuthRestAPIProtocol, ProfileRestAPIProtocol, CandidateRestAPIProtocol, ChatRestAPIProtocol {
    
    // MARK: Login
    func login(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let url = self.formatUrl(path: "login", networkParameters: params)
        AuthRestAPI(url: url).login(params: params, completion: completion)
    }
    
    // MARK: Login with fb
    func loginWithFB(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let url = self.formatUrl(path: "login/facebook", networkParameters: params)
        AuthRestAPI(url: url).loginWithFB(params: params, completion: completion)
    }
    
    // MARK: Register
    func register(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let url = self.formatUrl(path: "register", networkParameters: params)
        AuthRestAPI(url: url).register(params: params, completion: completion)
    }
    
    // MARK: Refresh token
    func refreshToken(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()){
        let url = self.formatUrl(path: "token", networkParameters: params)
        AuthRestAPI(url: url).refreshToken(params: params, completion: completion)
    }
    
    // MARK: Fetch profile
    func fetchProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()){
        let url = self.formatUrl(path: "profile", networkParameters: params)
        ProfileRestAPI(url: url).fetchProfile(params: params, completion: completion)
    }
    
    // MARK: Create profile
    func createProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()) {
        let url = self.formatUrl(path: "profile/basic", networkParameters: params)
        ProfileRestAPI(url: url).createProfile(params: params, completion: completion)
    }
    
    // MARK: Fetch candidates
    func fetchCandidates(params: NetworkParameters?, completion: @escaping ([CandidateResponse]?, Error?) -> ()){
        let url = self.formatUrl(path: "discover", networkParameters: params)
        CandidateRestAPI(url: url).fetchCandidates(params: params, completion: completion)
    }
    
    // MARK: Fetch candidate profile
    func fetchCandidateProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()){
        let candidateId: String = params?[.path, .candidateId(DefaultString)] ?? ""
        let url = self.formatUrl(path: "users/\(candidateId)", networkParameters: params)
        CandidateRestAPI(url: url).fetchCandidateProfile(params: params, completion: completion)
    }
    
    // MARK: Fetch candidate profile
    func evaluate(params: NetworkParameters?, completion: @escaping (EvaluationResponse?, Error?) -> ()) {
        let url = self.formatUrl(path: "evaluate", networkParameters: params)
        CandidateRestAPI(url: url).evaluate(params: params, completion: completion)
    }
    
    // MARK: Fetch chats
    func fetchChats(params: NetworkParameters?, completion: @escaping ([ChatResponse]?, Error?) -> ()){
        let url = self.formatUrl(path: "chats", networkParameters: params)
        ChatRestAPI(url: url).fetchChats(params: params, completion: completion)
    }
    
    // MARK: Fetch chat messages
    func fetchChatMessages(params: NetworkParameters?, completion: @escaping ([MessageChatResponse]?, Error?) -> ()){
        let chatId: String = params?[.path, .chatId(DefaultString)] ?? ""
        let url = self.formatUrl(path: "chats/\(chatId)/messages", networkParameters: params)
        ChatRestAPI(url: url).fetchChatMessages(params: params, completion: completion)
    }
    
    // MARK: Send chat message
    func sendChatMessage(params: NetworkParameters?, completion: @escaping (MessageChatResponse?, Error?) -> ()){
        let chatId: String = params?[.path, .chatId(DefaultString)] ?? ""
        let url = self.formatUrl(path: "chats/\(chatId)/messages", networkParameters: params)
        ChatRestAPI(url: url).sendChatMessage(params: params, completion: completion)
    }
}
