//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Alamofire

class AuthAdapter: RequestAdapter {
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        if let accessToken = UserDefaultsRepository().auth?.accessToken {
            urlRequest.setValue(accessToken, forHTTPHeaderField: "auth")
        }
        
        return urlRequest
    }
}
