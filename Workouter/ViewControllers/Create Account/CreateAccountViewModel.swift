//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol CreateAccountViewModelProtocol: class {
    func attemptDownloadProfile()
    func attemptCreateAccount(name: String, firstName: String, imageUrls: [String], age: Int, gender: Gender, preference: Preference, city: String) throws
    func cancelCreateAccount()
}

final class CreateAccountViewModel: BaseViewModel, CreateAccountViewModelProtocol {
    private let profileController: ProfileController
    
    required init(injector: Injector) {
        self.profileController = injector.profileController
        super.init(injector: injector)
    }
}

// MARK: Attempt download profile
extension CreateAccountViewModel {
    func attemptDownloadProfile() {
        self.profileController.fetchProfile()
    }
}

// MARK: Attempt create account
extension CreateAccountViewModel {
    func attemptCreateAccount(name: String, firstName: String, imageUrls: [String], age: Int, gender: Gender, preference: Preference, city: String) throws {
        guard age >= MinAge && age <= MaxAge &&
            gender != .unknown &&
            preference != .unknown else {
            throw MeetDayError.appError(.createAccount)
        }
        self.profileController.createProfile(name: name, firstName: firstName, imageUrls: imageUrls, age: age, gender: gender, preference: preference, city: city)
        
    }
}

// MARK: Cancel create account
extension CreateAccountViewModel {
    func cancelCreateAccount() {
        self.profileController.logout()
    }
}
