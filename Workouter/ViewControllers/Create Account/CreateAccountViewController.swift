//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import ObjectMapper

protocol CreateAccountViewControllerProtocol: class {
    func cancelCreateAccount()
    func createAccount()
    func goToMain()
}

class CreateAccountViewController: BaseViewController, ViewControllerType, CreateAccountViewControllerProtocol {
    private var cancelCreateAccountButton: UIBarButtonItem!
    @IBOutlet weak var createAccountButton: UIBarButtonItem!
    private var progressHandler = ProgressHandler()
    
    // MARK: View model
    var viewModel: CreateAccountViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
    
    // MARK: Actions
    @IBAction func createAccount(_ sender: Any) {
        self.createAccount()
    }
}

// MARK: Prepare UI
extension CreateAccountViewController {
    func prepareUI() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let view = UIView()
        let button = UIButton(type: .system)
        button.semanticContentAttribute = .forceLeftToRight
        button.setImage(UIImage(named: "ic_back"), for: .normal)
        button.setTitle(" Cancel", for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 17)
        button.addTarget(self, action: #selector(self.cancelCreateAccount), for: .touchUpInside)
        button.sizeToFit()
        // To make appear like back button
        button.transform = CGAffineTransform(translationX: -10, y: 0)
        view.addSubview(button)
        view.frame = button.bounds
        self.cancelCreateAccountButton = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = self.cancelCreateAccountButton
    }
}

// MARK: Prepare View Model
extension CreateAccountViewController {
    func prepareViewModel() {
        self.viewModel.attemptDownloadProfile()
        self.viewModel.eventObserver.observeProfile { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let profile):
                Logger.log("profile id: \(profile.id ?? "Not found")")
                if !profile.isProfileBasic {
                    self?.goToMain()
                    self?.progressHandler.hide()
                }
            case .error(let error):
                Logger.log("error: \(error)")
                if case let .dbError(databaseError)? = (error as? MeetDayError),
                    case .emptyProfile = databaseError {
                    self?.navigationController?.popViewController(animated: true)
                    return
                }
                self?.progressHandler.hideWithError(error, origin: self, title: "Create account error")
            }
        }
    }
}

// MARK: Cancel create account
extension CreateAccountViewController {
    @objc func cancelCreateAccount() {
        self.progressHandler.show(button: self.cancelCreateAccountButton, buttonsToDisable: [self.createAccountButton])
        self.viewModel.cancelCreateAccount()
    }
}

// MARK: Create account
extension CreateAccountViewController {
    func createAccount() {
        self.progressHandler.show(button: self.createAccountButton, buttonsToDisable: [self.cancelCreateAccountButton])
        do {
            try self.viewModel.attemptCreateAccount(name: "Name1", firstName: "firstName2", imageUrls: ["https://www.w3schools.com/html/pulpitrock.jpg", "https://www.w3schools.com/html/pulpitrock.jpg"], age: 22, gender: .female, preference: .all, city: "City3")
        } catch (let error) {
            self.progressHandler.hideWithError(error, origin: self, title: "Create account")
        }
    }
}

// MARK: Go to main
extension CreateAccountViewController {
    func goToMain() {
        Navigator.shared.perform(self, segue: .createAccountToMainSegue)
    }
}
