//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class Navigator {
    static let shared = Navigator()
    
    enum Segue: String {
        case launchToLoginSegue
        case launchToMainSegue
        case launchToCreateAccountSegue
        case loginToMainSegue
        case loginToLoginWithMDSegue
        case loginToRegisterSegue
        case loginToCreateAccountSegue
        case loginWithMDToMainSegue
        case loginWithMDToCreateAccountSegue
        case createAccountToMainSegue
        case discoverToCandidateSegue
        case chatListToChatSegue
    }
    
    enum Storyboard: String {
        case Discover
        case Chat
        case Profile
        case Settings
    }
    
    enum Identifier: String {
        case DiscoverViewController
        case ChatListViewController
        case ProfileViewController
        case SettingsViewController
    }
}

// MARK: Error Log
extension Navigator {
    private func logPerformSegueError(_ segue: Segue) {
        Logger.log("could not perform \(segue)")
    }

    private func logPrepareForSegueError(_ segue: UIStoryboardSegue) {
        Logger.log("could not prepare for \(segue)")
    }
    
    private func logInstantiateError(_ identifier: Identifier) {
        Logger.log("could not instantiate \(identifier)")
    }
}

// MARK: Perform segues
extension Navigator {
    func perform(_ viewController: UIViewController?, segue: Segue) {
        if self.canPerformSegue(viewController, segue: segue) {
            viewController?.performSegue(withIdentifier: segue.rawValue, sender: viewController)
        } else {
            self.logPerformSegueError(segue)
        }
    }
    
    private func canPerformSegue(_ viewController: UIViewController?, segue: Segue) -> Bool {
        switch segue {
        case .launchToLoginSegue,
             .launchToMainSegue,
             .launchToCreateAccountSegue:
            return viewController is LaunchViewController
        case .loginToMainSegue,
             .loginToLoginWithMDSegue,
             .loginToRegisterSegue,
             .loginToCreateAccountSegue:
            return viewController is MainLoginViewController
        case .loginWithMDToMainSegue,
             .loginWithMDToCreateAccountSegue:
            return viewController is LoginWithMDViewController
        case .createAccountToMainSegue:
            return viewController is CreateAccountViewController
        case .discoverToCandidateSegue:
            return viewController is DiscoverViewController
        case .chatListToChatSegue:
            return viewController is ChatListViewController
        }
    }
}

// MARK: Prepare segues
extension Navigator {
    func prepare(_ origin: UIViewController?, for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueIdentifier = segue.identifier, let segueEnum: Segue = Segue(rawValue: segueIdentifier) else {
            self.logPrepareForSegueError(segue)
            return
        }
        
        self.constructViewModel(origin, destination: segue.destination, segue: segueEnum)
    }
}

// MARK: Construct view model
extension Navigator {
    private func constructViewModel(_ origin: UIViewController?, destination: UIViewController?, segue: Segue) {
        switch segue {
        case .launchToLoginSegue:
            if let origin = origin as? LaunchViewController,
                let destination = destination as? MainLoginViewController {
                origin.viewModel.provide(ProvisionType.MainLogin, to: destination)
            }
        case .launchToMainSegue:
            if let origin = origin as? LaunchViewController,
                let destination = destination as? BottomTabBarController {
                origin.viewModel.provide(ProvisionType.BottomTabBar, to: destination)
            }
        case .launchToCreateAccountSegue:
            if let origin = origin as? LaunchViewController,
                let destination = destination as? CreateAccountViewController {
                origin.viewModel.provide(ProvisionType.CreateAccount, to: destination)
            }
        case .loginToLoginWithMDSegue:
            if let origin = origin as? MainLoginViewController,
                let destination = destination as? LoginWithMDViewController {
                origin.viewModel.provide(ProvisionType.LoginWithMD, to: destination)
            }
        case .loginToRegisterSegue:
            if let origin = origin as? MainLoginViewController,
                let destination = destination as? RegisterViewController {
                origin.viewModel.provide(ProvisionType.Register, to: destination)
                destination.delegate = origin
            }
        case .loginToMainSegue:
            if let origin = origin as? MainLoginViewController,
                let destination = destination as? BottomTabBarController {
                origin.viewModel.provide(ProvisionType.BottomTabBar, to: destination)
            }
        case .loginToCreateAccountSegue:
            if let origin = origin as? MainLoginViewController,
                let destination = destination as? CreateAccountViewController {
                origin.viewModel.provide(ProvisionType.CreateAccount, to: destination)
            }
        case .loginWithMDToMainSegue:
            if let origin = origin as? LoginWithMDViewController,
                let destination = destination as? BottomTabBarController {
                origin.viewModel.provide(ProvisionType.BottomTabBar, to: destination)
            }
        case .loginWithMDToCreateAccountSegue:
            if let origin = origin as? LoginWithMDViewController,
                let destination = destination as? CreateAccountViewController {
                origin.viewModel.provide(ProvisionType.CreateAccount, to: destination)
            }
        case .createAccountToMainSegue:
            if let origin = origin as? CreateAccountViewController,
                let destination = destination as? BottomTabBarController {
                origin.viewModel.provide(ProvisionType.BottomTabBar, to: destination)
            }
        case .discoverToCandidateSegue:
            if let origin = origin as? DiscoverViewController,
                let destination = destination as? ProfileViewController,
                let candidate = origin.viewModel?.selectedCandidate {
                destination.hidesBottomBarWhenPushed = true
                origin.viewModel.provide(ProvisionType.CandidateProfile, candidate: candidate, to: destination)
            }
        case .chatListToChatSegue:
            if let origin = origin as? ChatListViewController,
                let destination = destination as? ChatViewController,
                let chat = origin.viewModel.selectedChat {
                origin.viewModel.provide(ProvisionType.Chat, chat: chat, to: destination)
            }
        }
    }
}

// MARK: Instantiate view controller
extension Navigator {
    func instantiateViewController(_ origin: UIViewController?, withIdentifier: Identifier) -> BaseNavigationController? {
        switch withIdentifier {
        case .DiscoverViewController:
            let storyboard = UIStoryboard(name: Storyboard.Discover.rawValue, bundle: Bundle.main)
            if let origin = origin as? BottomTabBarController,
                let viewController = storyboard.instantiateViewController(withIdentifier: withIdentifier.rawValue) as? DiscoverViewController {
                origin.viewModel.provide(ProvisionType.Discover, to: viewController)
                return BaseNavigationController(rootViewController: viewController)
            }
        case .ChatListViewController:
            let storyboard = UIStoryboard(name: Storyboard.Chat.rawValue, bundle: Bundle.main)
            if let origin = origin as? BottomTabBarController,
                let viewController = storyboard.instantiateViewController(withIdentifier: withIdentifier.rawValue) as? ChatListViewController {
                origin.viewModel.provide(ProvisionType.ChatList, to: viewController)
                return BaseNavigationController(rootViewController: viewController)
            }
        case .ProfileViewController:
            let storyboard = UIStoryboard(name: Storyboard.Profile.rawValue, bundle: Bundle.main)
            if let origin = origin as? BottomTabBarController,
                let viewController = storyboard.instantiateViewController(withIdentifier: withIdentifier.rawValue) as? ProfileViewController {
                origin.viewModel.provide(ProvisionType.Profile, to: viewController)
                return BaseNavigationController(rootViewController: viewController)
            }
        case .SettingsViewController:
            let storyboard = UIStoryboard(name: Storyboard.Settings.rawValue, bundle: Bundle.main)
            if let origin = origin as? BottomTabBarController,
                let viewController = storyboard.instantiateViewController(withIdentifier: withIdentifier.rawValue) as? SettingsViewController {
                origin.viewModel.provide(ProvisionType.Settings, to: viewController)
                return BaseNavigationController(rootViewController: viewController)
            }
        }
        self.logInstantiateError(withIdentifier)
        return nil
    }
}
