//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class BaseNavigationController: UINavigationController {}

// MARK: Status bar style
extension BaseNavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: Navigation bar style
extension BaseNavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.barTintColor = UIColor.primary()
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationBar.tintColor = UIColor.white
    }
}
