//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class ProgressHandler {
    private let tag = 808404
    var progressing: Bool = false
    var button: AnyObject?
    var buttonsToDisable: [AnyObject] = []
    var previousDisabledColor: UIColor?
    var previousTitle: String?
    
    func show(button: AnyObject, color: UIColor? = nil, buttonsToDisable: [AnyObject] = []) {
        if let uiButton = button as? UIButton {
            self.show(button: uiButton, color: color)
        } else if let barButton = button as? UIBarButtonItem {
            self.show(barButton: barButton, color: color)
        }
        self.buttonsToDisable = buttonsToDisable
        self.changeButtons(buttonsToDisable, enabled: false)
    }
    
    private func show(button: UIButton, color: UIColor? = nil) {
        self.progressing = true
        self.button = button
        button.isEnabled = false
        self.previousDisabledColor = button.titleColor(for: .disabled)
        let currentColor = button.titleColor(for: .normal)
        button.setTitleColor(UIColor.clear, for: .disabled)
        let indicator = UIActivityIndicatorView()
        let buttonHeight = button.bounds.size.height
        let buttonWidth = button.bounds.size.width
        indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
        indicator.tag = tag
        indicator.color = color ?? currentColor
        button.addSubview(indicator)
        indicator.startAnimating()
    }
    
    private func show(barButton: UIBarButtonItem, color: UIColor? = nil) {
        self.progressing = true
        self.button = barButton
        self.previousTitle = barButton.title
        barButton.isEnabled = false
        barButton.title = "Loading"
    }
    
    private func changeButtons(_ buttonsToChange: [AnyObject] = [], enabled: Bool) {
        for buttonToDisable in buttonsToDisable {
            if let uiButton = buttonToDisable as? UIButton {
                uiButton.isEnabled = enabled
            } else if let barButton = buttonToDisable as? UIBarButtonItem {
                barButton.isEnabled = enabled
            }
        }
    }
    
    func hide() {
        guard self.progressing else {
            return
        }
        self.progressing = false
        if let uiButton = self.button as? UIButton {
            self.hide(button: uiButton)
        } else if let barButton = self.button as? UIBarButtonItem {
            self.hide(barButton: barButton)
        }
        self.changeButtons(self.buttonsToDisable, enabled: true)
    }
    
    func hideWithError(_ error: Error, origin: UIViewController?, title: String) {
        guard self.progressing else {
            return
        }
        self.hide()
        AlertHandler.showError(origin: origin, title: title, error: error)
    }
    
    private func hide(button: UIButton) {
        button.isEnabled = true
        button.setTitleColor(self.previousDisabledColor, for: .disabled)
        
        if let indicator = button.viewWithTag(tag) as? UIActivityIndicatorView {
            indicator.stopAnimating()
            indicator.removeFromSuperview()
        }
    }
    
    private func hide(barButton: UIBarButtonItem) {
        barButton.isEnabled = true
        barButton.title = self.previousTitle
    }
}
