//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class BaseViewController: UIViewController {}

// MARK: Auto rotate
extension BaseViewController {
    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

// MARK: Prepare segues
extension BaseViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        Navigator.shared.prepare(self, for: segue, sender: sender)
    }
}

// MARK: Navigation title
extension BaseViewController {
    func setNavigationTitle(_ title: String?) {
        self.navigationController?.navigationBar.topItem?.title = title
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
    }
    
    func setNavigationTitlePushedView(_ title: String?) {
        self.navigationItem.title = title
    }
}

// MARK: Bottom tab bar controller
extension BaseViewController {
    var bottomTabBarController: BottomTabBarController? {
        return self.tabBarController as? BottomTabBarController
    }
}
