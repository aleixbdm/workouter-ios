//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ImageSlideshow

protocol ProfileViewModelProtocol: class {
    var candidate: Candidate? { get }
    var profile: Profile? { get set }
    var images: [AlamofireSource] { get }
    var name: String { get }
    var genderImage: UIImage? { get }
    func skillValue(category: SkillCategory) -> CGFloat
    func attemptDownloadProfile()
    func attemptDownloadCandidateProfile()
}

final class ProfileViewModel: BaseViewModel, ProfileViewModelProtocol {
    private let profileController: ProfileController
    private let candidateController: CandidateController
    private(set) var candidate: Candidate?
    var profile: Profile?

    required init(injector: Injector) {
        self.profileController = injector.profileController
        self.candidateController = injector.candidateController
        super.init(injector: injector)
    }

    convenience init(injector: Injector, candidate: Candidate? = nil) {
        self.init(injector: injector)
        self.candidate = candidate
    }
}

// MARK: Images
extension ProfileViewModel {
    var images: [AlamofireSource] {
        return self.profile?.imageUrls.flatMap({AlamofireSource(urlString:$0)}) ??
            self.candidate?.imageUrls.flatMap({AlamofireSource(urlString:$0)}) ??
            []
    }
}

// MARK: Name
extension ProfileViewModel {
    var name: String {
        let name: String = self.profile?.name ??
            self.candidate?.name ??
            ""
        let age: Int = self.profile?.age.value ??
            self.candidate?.age ?? 18
        return "\(name), \(age)"
    }
}

// MARK: Gender image
extension ProfileViewModel {
    var genderImage: UIImage? {
        switch self.profile?.genderType {
        case .male?:
            return #imageLiteral(resourceName: "ic_gender_male")
        case .female?:
            return #imageLiteral(resourceName: "ic_gender_female")
        default:
            return nil
        }
    }
}

// MARK: Skill value
extension ProfileViewModel {
    func skillValue(category: SkillCategory) -> CGFloat {
        let skills: [Skill] = self.profile?.skills.map({$0}) ?? self.candidate?.skills ?? []
        return CGFloat(skills.first(where: {$0.categoryType == category})?.value.value ?? 0)
    }
}

// MARK: Attempt download profile
extension ProfileViewModel {
    func attemptDownloadProfile() {
        self.profileController.fetchProfile()
    }
}

// MARK: Attempt download candidate profile
extension ProfileViewModel {
    func attemptDownloadCandidateProfile() {
        guard let candidateId = self.candidate?.id else { return }
        self.candidateController.fetchCandidateProfile(candidateId: candidateId)
    }
}
