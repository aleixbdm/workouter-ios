//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit
import ImageSlideshow
import RangeSeekSlider

protocol ProfileViewControllerProtocol: class {
    func goEditProfile()
}

class ProfileViewController: BaseViewController, ViewControllerType, ProfileViewControllerProtocol {
    
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var generalInformationLabel: UILabel!
    @IBOutlet weak var currentJobLabel: UILabel!
    @IBOutlet weak var studiesLabel: UILabel!
    @IBOutlet weak var favouriteSongLabel: UILabel!
    @IBOutlet weak var sportsSlider: RangeSeekSlider!
    @IBOutlet weak var geekSlider: RangeSeekSlider!
    @IBOutlet weak var danceSlider: RangeSeekSlider!
    @IBOutlet weak var travelSlider: RangeSeekSlider!
    @IBOutlet weak var petsSlider: RangeSeekSlider!
    @IBOutlet weak var greenSlider: RangeSeekSlider!
    @IBOutlet weak var editButton: MDButtonText!
    @IBOutlet weak var editButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var likeButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dislikeButtonHeightConstraint: NSLayoutConstraint!
    
    // MARK: View model
    var viewModel: ProfileViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
}

// MARK: Prepare UI
extension ProfileViewController {
    func prepareUI() {
        if self.viewModel?.candidate != nil {
            self.prepareCandidateView()
        } else if self.viewModel?.profile != nil {
            self.prepareProfileView()
        }
        self.prepareImageSlider()
        self.prepareName()
        self.prepareGender()
        self.prepareGeneralInformation()
        self.prepareCurrentJobLabel()
        self.prepareStudiesLabel()
        self.prepareFavouriteSongLabel()
        self.prepareSliders()
    }
    
    private func prepareProfileView() {
        self.likeButtonHeightConstraint.constant = 0
        self.dislikeButtonHeightConstraint.constant = 0
    }
    
    private func prepareCandidateView() {
        self.setNavigationTitlePushedView(self.viewModel.candidate?.name)
        self.editButtonHeightConstraint.constant = 0
    }
    
    private func prepareImageSlider() {
        self.imageSlider.setImageInputs(self.viewModel.images)
        self.imageSlider.activityIndicator = DefaultActivityIndicator()
        self.imageSlider.contentScaleMode = .scaleToFill
        self.imageSlider.layoutSubviews()
    }
    
    private func prepareName() {
        self.nameLabel.text = self.viewModel.name
    }
    
    private func prepareGender() {
        self.genderImageView.image = self.viewModel.genderImage
    }
    
    private func prepareGeneralInformation() {
        self.generalInformationLabel.text = self.viewModel.profile?.generalInformation
    }
    
    private func prepareCurrentJobLabel() {
        self.currentJobLabel.text = self.viewModel.profile?.generalInformation
    }
    
    private func prepareStudiesLabel() {
        self.studiesLabel.text = self.viewModel.profile?.studies
    }
    
    private func prepareFavouriteSongLabel() {
        self.favouriteSongLabel.text = self.viewModel.profile?.favouriteSong
    }
    
    private func prepareSliders() {
        self.sportsSlider.selectedMaxValue = self.viewModel.skillValue(category: .sports)
        self.geekSlider.selectedMaxValue = self.viewModel.skillValue(category: .geek)
        self.danceSlider.selectedMaxValue = self.viewModel.skillValue(category: .dance)
        self.travelSlider.selectedMaxValue = self.viewModel.skillValue(category: .travel)
        self.petsSlider.selectedMaxValue = self.viewModel.skillValue(category: .pets)
        self.greenSlider.selectedMaxValue = self.viewModel.skillValue(category: .green)
    }
}

// MARK: Prepare view model
extension ProfileViewController {
    func prepareViewModel(){
        if self.viewModel.candidate == nil {
            self.setNavigationTitle("Profile")
            self.viewModel.attemptDownloadProfile()
            self.viewModel.eventObserver.observeProfile { [weak self] (eventProperty) in
                switch eventProperty.event {
                case .success(let profile):
                    Logger.log("profile id: \(profile.id ?? "Not found")")
                    self?.viewModel.profile = profile
                    self?.prepareUI()
                case .error(let error):
                    Logger.log("error: \(error)")
                }
            }
        } else {
            self.viewModel.attemptDownloadCandidateProfile()
            self.viewModel.eventObserver.observeCandidateProfile { [weak self] (eventProperty) in
                switch eventProperty.event {
                case .success(let candidateProfile):
                    Logger.log("candidateProfile id: \(candidateProfile.id ?? "Not found")")
                    self?.viewModel.profile = candidateProfile
                    self?.prepareUI()
                case .error(let error):
                    Logger.log("error: \(error)")
                }
            }
        }
    }
}

// MARK: ProfileViewControllerProtocol
extension ProfileViewController {
    func goEditProfile() {
        // TODO
    }
}
