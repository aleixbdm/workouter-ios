//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto
import ChattoAdditions

public protocol ChatMessageModelProtocol: MessageModelProtocol {
    var status: MessageStatus { get set }
}

public class ChatMessageStatus {

    public var onMessageChanged: ((_ message: ChatMessageModelProtocol) -> Void)?

    public func updateMessage(_ message: ChatMessageModelProtocol, status: MessageStatus) {
        if message.status != status {
            message.status = status
            self.notifyMessageChanged(message)
        }
    }

    private func notifyMessageChanged(_ message: ChatMessageModelProtocol) {
        self.onMessageChanged?(message)
    }
}
