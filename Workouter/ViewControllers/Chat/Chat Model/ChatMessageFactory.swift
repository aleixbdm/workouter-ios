//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto
import ChattoAdditions

class ChatMessageFactory {
    class func makeTextMessage(_ message: MessageChat, isIncoming: Bool) -> ChatTextMessageModel {
        let text = message.messageText ?? ""
        let messageModel = self.makeMessageModel(message, isIncoming: isIncoming, type: TextMessageModel<MessageModel>.chatItemType)
        let textMessageModel = ChatTextMessageModel(messageModel: messageModel, text: text)
        return textMessageModel
    }
    
    class func makeTextMessage(_ uid: String, senderUserId: String, text: String) -> ChatTextMessageModel {
        let messageModel = self.makeMessageModel(uid, senderUserId: senderUserId, type: TextMessageModel<MessageModel>.chatItemType)
        let textMessageModel = ChatTextMessageModel(messageModel: messageModel, text: text)
        return textMessageModel
    }

    private class func makeMessageModel(_ message: MessageChat, isIncoming: Bool, type: String) -> MessageModel {
        let uid = message.id ?? UUID().uuidString
        let senderId = message.senderUserId ?? UUID().uuidString
        let date = message.createdAt ?? Date()
        return MessageModel(uid: uid, senderId: senderId, type: type, isIncoming: isIncoming, date: date, status: .success)
    }
    
    private class func makeMessageModel(_ uid: String, senderUserId: String, type: String) -> MessageModel {
        return MessageModel(uid: uid, senderId: senderUserId, type: type, isIncoming: false, date: Date(), status: .sending)
    }
}

extension TextMessageModel {
    static var chatItemType: ChatItemType {
        return "text"
    }
}
