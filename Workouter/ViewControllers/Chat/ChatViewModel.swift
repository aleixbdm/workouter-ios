//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol ChatViewModelProtocol: class {
    var chat: Chat? { get }
    var userId: String? { get }
    func attemptDownloadChatMessages()
    func attemptSendMessage(message: String)
}

final class ChatViewModel: BaseViewModel, ChatViewModelProtocol {
    private let chatController: ChatController
    private let profileController: ProfileController
    private(set) var chat: Chat?

    required init(injector: Injector) {
        self.chatController = injector.chatController
        self.profileController = injector.profileController
        super.init(injector: injector)
    }

    convenience init(injector: Injector, chat: Chat) {
        self.init(injector: injector)
        self.chat = chat
    }
    
    // MARK: Close
    override func close() {
        super.close()
        self.chatController.stopRefreshChatMessages()
        self.chatController.discardChatMessages()
    }
}

// MARK: User id
extension ChatViewModel {
    var userId: String? {
        return self.profileController.auth?.userId
    }
}

// MARK: Attempt download chat messages
extension ChatViewModel {
    func attemptDownloadChatMessages() {
        guard let chatId = self.chat?.id else { return }
        self.chatController.fetchChatMessages(chatId: chatId)
    }
}

// MARK: Attempt send message
extension ChatViewModel {
    func attemptSendMessage(message: String) {
        guard let chatId = self.chat?.id else { return }
        self.chatController.sendChatMessage(chatId: chatId, messageText: message)
    }
}
