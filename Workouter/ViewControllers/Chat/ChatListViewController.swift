//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol ChatListViewControllerProtocol: class {
    func goChat(_ chat: Chat)
}

class ChatListViewController: BaseTableViewController, ViewControllerType, ChatListViewControllerProtocol {
    
    // MARK: View model
    var viewModel: ChatListViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationTitle("Chats")
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
}

// MARK: Prepare UI
extension ChatListViewController {
    func prepareUI(){
        // Nothing to do
    }
}

// MARK: Prepare view model
extension ChatListViewController {
    func prepareViewModel(){
        self.viewModel.attemptDownloadChats()
        self.viewModel.eventObserver.observeChats { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let chats):
                Logger.log("chats count: \(chats.count)")
                self?.viewModel.dataSource = chats
                self?.tableView.reloadData()
            case .error(let error):
                Logger.log("error: \(error)")
            }
        }
    }
}

// MARK: - Table view data source
extension ChatListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ChatTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ChatTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ChatTableViewCell.")
        }
        
        let chat = self.viewModel.dataSource[indexPath.row]
        cell.loadData(chat: chat)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.goChat(self.viewModel.dataSource[indexPath.row])
    }
}

// MARK: ChatListViewControllerProtocol
extension ChatListViewController {
    func goChat(_ chat: Chat) {
        self.viewModel.selectedChat = chat
        Navigator.shared.perform(self, segue: .chatListToChatSegue)
    }
}
