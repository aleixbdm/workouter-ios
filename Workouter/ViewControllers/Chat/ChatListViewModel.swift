//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol ChatListViewModelProtocol: class {
    var dataSource: [Chat] { get set }
    var selectedChat: Chat? { get set }
    func attemptDownloadChats()
}

final class ChatListViewModel: BaseViewModel, ChatListViewModelProtocol {
    private let chatController: ChatController
    var dataSource: [Chat] = []
    var selectedChat: Chat?
    
    required init(injector: Injector) {
        self.chatController = injector.chatController
        super.init(injector: injector)
    }
    
    // MARK: Close
    override func close() {
        super.close()
        self.chatController.stopRefreshChats()
    }
}

// MARK: Attempt download chats
extension ChatListViewModel {
    func attemptDownloadChats() {
        self.chatController.fetchChats()
    }
}
