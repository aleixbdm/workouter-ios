//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto

class ChatDataSource: ChatDataSourceProtocol {
    let preferredMaxWindowSize = 500
    private let userId: String

    var slidingWindow: SlidingDataSource<ChatItemProtocol>!
    init(userId: String, pageSize: Int) {
        self.userId = userId
        self.slidingWindow = SlidingDataSource(items: [], pageSize: pageSize)
    }

    init(userId: String, messages: [ChatItemProtocol], pageSize: Int) {
        self.userId = userId
        self.slidingWindow = SlidingDataSource(items: messages, pageSize: pageSize)
    }

    lazy var messageStatus: ChatMessageStatus = {
        let sender = ChatMessageStatus()
        sender.onMessageChanged = { [weak self] (message) in
            guard let sSelf = self else { return }
            sSelf.delegate?.chatDataSourceDidUpdate(sSelf)
        }
        return sender
    }()

    var hasMoreNext: Bool {
        return self.slidingWindow.hasMore()
    }

    var hasMorePrevious: Bool {
        return self.slidingWindow.hasPrevious()
    }

    var chatItems: [ChatItemProtocol] {
        return self.slidingWindow.itemsInWindow
    }

    weak var delegate: ChatDataSourceDelegateProtocol?

    func loadNext() {
        self.slidingWindow.loadNext()
        self.slidingWindow.adjustWindow(focusPosition: 1, maxWindowSize: self.preferredMaxWindowSize)
        self.delegate?.chatDataSourceDidUpdate(self, updateType: .pagination)
    }

    func loadPrevious() {
        self.slidingWindow.loadPrevious()
        self.slidingWindow.adjustWindow(focusPosition: 0, maxWindowSize: self.preferredMaxWindowSize)
        self.delegate?.chatDataSourceDidUpdate(self, updateType: .pagination)
    }
    
    func addMessages(_ messages: [MessageChat]) {
        for message in messages {
            let item = ChatMessageFactory.makeTextMessage(message, isIncoming: message.senderUserId != userId)
            self.slidingWindow.insertItem(item, position: .bottom)
        }
        self.delegate?.chatDataSourceDidUpdate(self)
    }

    func addTextMessage(_ text: String) {
        let uid = UUID().uuidString
        let message = ChatMessageFactory.makeTextMessage(uid, senderUserId: self.userId, text: text)
        self.slidingWindow.insertItem(message, position: .bottom)
        self.delegate?.chatDataSourceDidUpdate(self)
    }
    
    func findSendingChatMessage() -> ChatMessageModelProtocol? {
        return self.chatItems.filter({($0 as? ChatTextMessageModel)?.status == .sending}).first as? ChatMessageModelProtocol
    }

    func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion:(_ didAdjust: Bool) -> Void) {
        let didAdjust = self.slidingWindow.adjustWindow(focusPosition: focusPosition, maxWindowSize: preferredMaxCount ?? self.preferredMaxWindowSize)
        completion(didAdjust)
    }
}
