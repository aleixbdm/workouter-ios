//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto
import ChattoAdditions

protocol ChatViewControllerProtocol: class {
    func sendMessage(_ message: String)
}

class ChatViewController: BaseChatViewController, ViewControllerType, ChatViewControllerProtocol {

    // MARK: Chatto components
    var messageStatus: ChatMessageStatus!
    
    var dataSource: ChatDataSource! {
        didSet {
            self.chatDataSource = self.dataSource
            self.messageStatus = self.dataSource.messageStatus
        }
    }
    
    lazy private var baseMessageHandler: BaseMessageHandler = {
        return BaseMessageHandler(chatViewModel: self.viewModel, messageStatus: self.messageStatus)
    }()
    
    // MARK: View model
    var viewModel: ChatViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareViewModel()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
    
    // MARK: Chat input
    var chatInputPresenter: BasicChatInputBarPresenter!
    override func createChatInputView() -> UIView {
        return self.chatInputView
    }
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        return self.presenterBuilders
    }
}

// MARK: Prepare UI
extension ChatViewController {
    func prepareUI(){
        self.navigationItem.title = self.viewModel.chat?.candidateUser?.name
        self.edgesForExtendedLayout = []
        self.view.backgroundColor = UIColor.chatBackground()
        
        self.chatItemsDecorator = ChatItemsDecorator()
    }
}

// MARK: Prepare view model
extension ChatViewController {
    func prepareViewModel() {
        self.viewModel.attemptDownloadChatMessages()
        self.viewModel.eventObserver.observeChatMessages { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let chatMessages):
                Logger.log("chatMessages count: \(chatMessages.count)")
                self?.dataSource.addMessages(chatMessages)
            case .error(let error):
                Logger.log("error: \(error)")
            }
        }
        self.viewModel.eventObserver.observeChatMessageSent { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let chatMessage):
                Logger.log("chatMessage sent: \(chatMessage.id ?? "Not found")")
                if let sendingChatMessage = self?.dataSource.findSendingChatMessage() {
                    self?.messageStatus.updateMessage(sendingChatMessage, status: .success)
                }
            case .error(let error):
                Logger.log("error: \(error)")
                if let sendingChatMessage = self?.dataSource.findSendingChatMessage() {
                    self?.messageStatus.updateMessage(sendingChatMessage, status: .failed)
                }
            }
        }
    }
}

// MARK: Chatto functions
extension ChatViewController {
    var chatInputView: ChatInputBar {
        let chatInputView = ChatInputBar.loadNib()
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonAppearance.title = NSLocalizedString("Send", comment: "")
        appearance.sendButtonAppearance.titleColors = [
            UIControlStateWrapper(state: .normal): UIColor.primary()
        ]
        appearance.textInputAppearance.placeholderText = NSLocalizedString("Type a message", comment: "")
        appearance.textInputAppearance.tintColor = UIColor.primary()
        self.chatInputPresenter = BasicChatInputBarPresenter(chatInputBar: chatInputView, chatInputItems: self.createChatInputItems(), chatInputBarAppearance: appearance)
        chatInputView.maxCharactersCount = 1000
        return chatInputView
    }
    
    var presenterBuilders: [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        let textMessagePresenter = TextMessagePresenterBuilder(
            viewModelBuilder: ChatTextMessageViewModelBuilder(),
            interactionHandler: ChatTextMessageHandler(baseHandler: self.baseMessageHandler)
        )
        
        let chatColor = BaseMessageCollectionViewCellDefaultStyle.Colors(
            incoming: UIColor.primaryLight(),
            outgoing: UIColor.white
        )
        
        let baseMessageStyle = BaseMessageCollectionViewCellDefaultStyle(colors: chatColor)
        
        let textStyle = TextMessageCollectionViewCellDefaultStyle.TextStyle(
            font: UIFont.systemFont(ofSize: 13),
            incomingColor: UIColor.white,
            outgoingColor: UIColor.black,
            incomingInsets: UIEdgeInsets(top: 10, left: 19, bottom: 10, right: 15),
            outgoingInsets: UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 19)
        )
        
        let textCellStyle: TextMessageCollectionViewCellDefaultStyle = TextMessageCollectionViewCellDefaultStyle(
            textStyle: textStyle,
            baseStyle: baseMessageStyle)
        
        textMessagePresenter.baseMessageStyle = baseMessageStyle
        textMessagePresenter.textCellStyle = textCellStyle
        
        return [
            ChatTextMessageModel.chatItemType: [textMessagePresenter],
            SendingStatusModel.chatItemType: [SendingStatusPresenterBuilder()],
            TimeSeparatorModel.chatItemType: [TimeSeparatorPresenterBuilder()]
        ]
    }
    
    func createChatInputItems() -> [ChatInputItemProtocol] {
        var items = [ChatInputItemProtocol]()
        items.append(self.createTextInputItem())
        return items
    }
    
    private func createTextInputItem() -> BaseTextChatInputItem {
        let item = BaseTextChatInputItem()
        item.textInputHandler = { [weak self] text in
            self?.sendMessage(text)
        }
        return item
    }
}

// MARK: Send message
extension ChatViewController {
    func sendMessage(_ message: String) {
        self.dataSource.addTextMessage(message)
        self.viewModel.attemptSendMessage(message: message)
    }
}
