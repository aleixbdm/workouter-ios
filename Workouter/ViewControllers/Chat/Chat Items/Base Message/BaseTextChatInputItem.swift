//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ChattoAdditions

class BaseTextChatInputItem: TextChatInputItem {
    override init(tabInputButtonAppearance: TabInputButtonAppearance = TabInputButtonAppearance(images: [:], size: nil)) {
        super.init(tabInputButtonAppearance: tabInputButtonAppearance)
    }
}
