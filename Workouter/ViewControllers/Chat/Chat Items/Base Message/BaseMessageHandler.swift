//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto
import ChattoAdditions

public protocol ChatMessageViewModelProtocol {
    var messageModel: ChatMessageModelProtocol { get }
}

class BaseMessageHandler {
    private let chatViewModel: ChatViewModel
    private let messageStatus: ChatMessageStatus

    init(chatViewModel: ChatViewModel, messageStatus: ChatMessageStatus) {
        self.chatViewModel = chatViewModel
        self.messageStatus = messageStatus
    }
    
    func userDidTapOnFailIcon(viewModel: ChatMessageViewModelProtocol) {
        Logger.log("userDidTapOnFailIcon")
        if let chatTextViewModel = viewModel as? ChatTextMessageViewModel {
            self.chatViewModel.attemptSendMessage(message: chatTextViewModel.text)
        }
        self.messageStatus.updateMessage(viewModel.messageModel, status: .sending)
    }
}
