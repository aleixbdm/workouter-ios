//
//  ChatTextMessageViewModel.swift
//

import ChattoAdditions

public class ChatTextMessageViewModel: TextMessageViewModel<ChatTextMessageModel>, ChatMessageViewModelProtocol {

    public override init(textMessage: ChatTextMessageModel, messageViewModel: MessageViewModelProtocol) {
        super.init(textMessage: textMessage, messageViewModel: messageViewModel)
    }

    public var messageModel: ChatMessageModelProtocol {
        return self.textMessage
    }
}

public class ChatTextMessageViewModelBuilder: ViewModelBuilderProtocol {
    public init() {}

    let messageViewModelBuilder = MessageViewModelDefaultBuilder()

    public func createViewModel(_ textMessage: ChatTextMessageModel) -> ChatTextMessageViewModel {
        let messageViewModel = self.messageViewModelBuilder.createMessageViewModel(textMessage)
        return ChatTextMessageViewModel(textMessage: textMessage, messageViewModel: messageViewModel)
    }

    public func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatTextMessageModel
    }
}
