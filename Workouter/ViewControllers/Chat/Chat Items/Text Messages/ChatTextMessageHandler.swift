//
//  ChatTextMessageHandler.swift
//

import ChattoAdditions

class ChatTextMessageHandler: BaseMessageInteractionHandlerProtocol {
    private let baseHandler: BaseMessageHandler
    init (baseHandler: BaseMessageHandler) {
        self.baseHandler = baseHandler
    }

    func userDidTapOnFailIcon(viewModel: ChatTextMessageViewModel, failIconView: UIView) {
        self.baseHandler.userDidTapOnFailIcon(viewModel: viewModel)
    }

    func userDidTapOnAvatar(viewModel: ChatTextMessageViewModel) {
        // Not needed
    }

    func userDidTapOnBubble(viewModel: ChatTextMessageViewModel) {
        // Not needed
    }

    func userDidBeginLongPressOnBubble(viewModel: ChatTextMessageViewModel) {
        // Not needed
    }

    func userDidEndLongPressOnBubble(viewModel: ChatTextMessageViewModel) {
        // Not needed
    }

    func userDidSelectMessage(viewModel: ChatTextMessageViewModel) {
        // Not needed
    }

    func userDidDeselectMessage(viewModel: ChatTextMessageViewModel) {
        // Not needed
    }
}
