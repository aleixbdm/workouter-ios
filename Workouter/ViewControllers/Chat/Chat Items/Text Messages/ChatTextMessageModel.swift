//
//  ChatTextMessageModel.swift
//

import ChattoAdditions

public class ChatTextMessageModel: TextMessageModel<MessageModel>, ChatMessageModelProtocol {
    public override init(messageModel: MessageModel, text: String) {
        super.init(messageModel: messageModel, text: text)
    }

    public var status: MessageStatus {
        get {
            return self._messageModel.status
        }
        set {
            self._messageModel.status = newValue
        }
    }
}
