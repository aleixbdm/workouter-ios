//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
 
    public func loadData(chat: Chat) {
        self.nameLabel.text = chat.candidateUser?.name
        if let imageUrl = chat.candidateUser?.imageUrl {
            self.photoImageView.setImage(url: imageUrl, rounded: true)
        }
        self.messageLabel.text = chat.lastMessage?.messageText
        self.timeLabel.text = chat.updatedAt?.timeAgo()
        self.counterLabel.text = "" // TODO: When backend knows read messages
    }
    
}
