//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol SettingsViewModelProtocol: class {
    func logout()
}

final class SettingsViewModel: BaseViewModel, SettingsViewModelProtocol {
    private let profileController: ProfileController
    
    required init(injector: Injector) {
        self.profileController = injector.profileController
        super.init(injector: injector)
    }
}

// MARK: Logout
extension SettingsViewModel {
    func logout() {
        self.profileController.logout()
    }
}
