//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol SettingsViewControllerProtocol: class {
    func logout()
}

class SettingsViewController: BaseViewController, ViewControllerType, SettingsViewControllerProtocol {
    
    // MARK: View model
    var viewModel: SettingsViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationTitle("Settings")
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
    
    // MARK: Actions
    @IBAction func logout(_ sender: Any) {
        self.logout()
    }
}

// MARK: Prepare UI
extension SettingsViewController {
    func prepareUI() {
        // TODO
    }
}

// MARK: Prepare view model
extension SettingsViewController {
    func prepareViewModel(){
        self.viewModel.eventObserver.observeProfile { (eventProperty) in
            switch eventProperty.event {
            case .success(let profile):
                Logger.log("profile id: \(profile.id ?? "Not found")")
            case .error(let error):
                Logger.log("error: \(error)")
                if case let .dbError(databaseError)? = (error as? MeetDayError),
                    case .emptyProfile = databaseError {
                    UIApplication.shared.restartApp(animated: true)
                }
            }
        }
    }
}

// MARK: SettingsViewControllerProtocol
extension SettingsViewController {
    func logout() {
        self.viewModel.logout()
    }
}
