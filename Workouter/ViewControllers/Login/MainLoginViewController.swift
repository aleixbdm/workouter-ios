//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import ObjectMapper

protocol MainLoginViewControllerProtocol: class {
    func loginWithFacebook()
    func goToMain()
    func goToLoginWithMD()
    func goToRegister()
    func goToCreateAccount()
}

class MainLoginViewController: BaseViewController, ViewControllerType, MainLoginViewControllerProtocol, RegisterViewControllerDelegate {
    @IBOutlet weak var loginWithFBButton: MDButton!
    @IBOutlet weak var loginWithMDButton: MDButtonText!
    @IBOutlet weak var createAccountButton: UIButton!
    private var progressHandler = ProgressHandler()
    
    // MARK: View model
    var viewModel: MainLoginViewModel!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
    
    // MARK: Actions
    @IBAction func loginWithFacebook(_ sender: Any) {
        self.loginWithFacebook()
    }
    
    @IBAction func loginWithMeetDay(_ sender: Any) {
        self.goToLoginWithMD()
    }
    
    @IBAction func createAccount(_ sender: Any) {
        self.goToRegister()
    }
}

// MARK: Prepare UI
extension MainLoginViewController {
    func prepareUI() {
        // Nothing to do
    }
}

// MARK: Prepare View Model
extension MainLoginViewController {
    func prepareViewModel() {
        self.viewModel.eventObserver.observeProfile { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let profile):
                Logger.log("profile id: \(profile.id ?? "Not found")")
                self?.progressHandler.hide()
                if profile.isProfileBasic {
                    self?.goToCreateAccount()
                } else {
                    self?.goToMain()
                }
            case .error(let error):
                Logger.log("error: \(error)")
                if case let .dbError(databaseError)? = (error as? MeetDayError),
                    case .emptyProfile = databaseError {
                    return
                }
                self?.progressHandler.hideWithError(error, origin: self, title: "Login error")
            }
        }
    }
}

// MARK: Login with facebook
extension MainLoginViewController {
    func loginWithFacebook() {
        self.progressHandler.show(button: self.loginWithFBButton, buttonsToDisable: [self.loginWithMDButton, self.createAccountButton])
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ .email, .publicProfile, .userBirthday ], viewController: self) { [weak self] (result) in
            switch result {
            case .failed(let error):
                Logger.log("Facebook Login Failed: \(error)")
                self?.progressHandler.hide()
            case .cancelled:
                Logger.log("User cancelled login.")
                self?.progressHandler.hide()
            case .success(_, _, let accessToken):
                Logger.log("Access Token \(accessToken)")
                self?.viewModel.attemptLoginWithFB(accessToken: accessToken.authenticationToken)
            }
        }
    }
}

// MARK: Go to main
extension MainLoginViewController {
    func goToMain() {
        Navigator.shared.perform(self, segue: .loginToMainSegue)
    }
}

// MARK: Go to login with meetday
extension MainLoginViewController {
    func goToLoginWithMD() {
        Navigator.shared.perform(self, segue: .loginToLoginWithMDSegue)
    }
}

// MARK: Go to register
extension MainLoginViewController {
    func goToRegister() {
        Navigator.shared.perform(self, segue: .loginToRegisterSegue)
    }
}

// MARK: Go to create account
extension MainLoginViewController {
    func goToCreateAccount() {
        Navigator.shared.perform(self, segue: .loginToCreateAccountSegue)
    }
}

// MARK: RegisterViewControllerDelegate
extension MainLoginViewController {
    func registered() {
        self.goToCreateAccount()
    }
}
