//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol RegisterViewControllerProtocol: class {
    func checkRegister()
    func dismissRegister()
    func endRegister()
}

protocol RegisterViewControllerDelegate: class {
    func registered()
}

class RegisterViewController: BaseViewController, ViewControllerType, RegisterViewControllerProtocol {
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var createButton: UIBarButtonItem!
    @IBOutlet weak var register_email: UITextField!
    @IBOutlet weak var register_password: UITextField!
    @IBOutlet weak var register_confirm_password: UITextField!
    @IBOutlet weak var register_accept_terms: CheckBox!
    
    @IBOutlet weak var email_validation_label: UILabel!
    @IBOutlet weak var password_validation_label: UILabel!
    @IBOutlet weak var confirm_password_validation_label: UILabel!
    @IBOutlet weak var accept_terms_validation_label: UILabel!

    private var errorChecker = ErrorChecker()
    private var progressHandler = ProgressHandler()
    weak var delegate: RegisterViewControllerDelegate?
    
    // MARK: View model
    var viewModel: RegisterViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
    
    // MARK: Actions
    @IBAction func cancelRegister(_ sender: UIBarButtonItem) {
        self.dismissRegister()
    }
    
    @IBAction func attemptRegister(_ sender: UIBarButtonItem) {
        self.checkRegister()
    }
    
    @IBAction func checkTerms(_ sender: Any) {
        if !self.register_accept_terms.isChecked {
            self.errorChecker.resetMessageError(validation_label: self.accept_terms_validation_label)
        }
    }
}

// MARK: Prepare UI
extension RegisterViewController {
    func prepareUI(){
        // Configure validation label
        self.email_validation_label.isHidden = true
        self.password_validation_label.isHidden = true
        self.confirm_password_validation_label.isHidden = true
        self.accept_terms_validation_label.isHidden = true
        
        // Configure textfields
        self.register_email.delegate = self
        self.register_password.delegate = self
        self.register_confirm_password.delegate = self
        self.register_email.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.register_password.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.register_confirm_password.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        
        // Optional enviroment credentials
        self.register_email.text = self.viewModel.enviromentEmail
        self.register_password.text = self.viewModel.enviromentPassword
        self.register_confirm_password.text = self.viewModel.enviromentPassword
        
        // Hide keyboard when tap screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

// MARK: Prepare View Model
extension RegisterViewController {
    func prepareViewModel() {
        self.viewModel.eventObserver.observeProfile { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let profile):
                Logger.log("profile id: \(profile.id ?? "Not found")")
                self?.progressHandler.hide()
                self?.endRegister()
            case .error(let error):
                Logger.log("error: \(error)")
                if case let .dbError(databaseError)? = (error as? MeetDayError),
                    case .emptyProfile = databaseError {
                    return
                }
                self?.progressHandler.hideWithError(error, origin: self, title: "Register error")
            }
        }
    }
}

// MARK: Check register
extension RegisterViewController {
    func checkRegister() {
        self.dismissKeyboard()
        
        guard let email = self.register_email.text,
            let password = self.register_password.text,
            let confirmPassword = self.register_confirm_password.text else {
                return
        }
        
        // Reset errors.
        self.errorChecker.resetMessageError(validation_label: self.email_validation_label)
        self.errorChecker.resetMessageError(validation_label: self.password_validation_label)
        self.errorChecker.resetMessageError(validation_label: self.confirm_password_validation_label)
        self.errorChecker.resetMessageError(validation_label: self.accept_terms_validation_label)
        
        var cancel: Bool = false
        var focusLabel: UITextField?
        
        let (email_valid, email_error_message) = self.validate(self.register_email)
        let (password_valid, password_error_message) = self.validate(self.register_password)
        let (confirm_password_valid, confirm_password_error_message) = self.validate(self.register_confirm_password)
        let accept_terms_valid = self.register_accept_terms.isChecked
        
        if let message = email_error_message, !email_valid {
            focusLabel = focusLabel ?? self.register_email
            self.errorChecker.setMessageError(isHidden: email_valid, message: message, validation_label: self.email_validation_label)
            cancel = true
        }
        
        if let message = password_error_message, !password_valid {
            focusLabel = focusLabel ?? self.register_password
            self.errorChecker.setMessageError(isHidden: password_valid, message: message, validation_label: self.password_validation_label)
            cancel = true
        }
        
        if let message = confirm_password_error_message, !confirm_password_valid {
            focusLabel = focusLabel ?? self.register_confirm_password
            self.errorChecker.setMessageError(isHidden: confirm_password_valid, message: message, validation_label: self.confirm_password_validation_label)
            cancel = true
        }
        
        if password != confirmPassword {
            self.errorChecker.setMessageError(isHidden: false, message: "The passwords don't match.", validation_label: self.confirm_password_validation_label)
            cancel = true
        }
        
        if !accept_terms_valid {
            self.errorChecker.setMessageError(isHidden: accept_terms_valid, message: "You must accept terms to register.", validation_label: self.accept_terms_validation_label)
            cancel = true
        }
        
        if cancel {
            focusLabel?.becomeFirstResponder()
        } else {
            self.progressHandler.show(button: self.createButton, buttonsToDisable: [self.cancelButton])
            self.viewModel?.attemptRegister(email: email, password: password)
        }
    }
}

// MARK: End register
extension RegisterViewController {
    func endRegister() {
        self.dismiss(animated: true) {
            self.delegate?.registered()
        }
    }
}

// MARK: Dismiss register
extension RegisterViewController {
    func dismissRegister() {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Textfield actions
extension RegisterViewController: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case self.register_email:
            self.errorChecker.resetMessageError(validation_label: self.email_validation_label)
            break
        case self.register_password:
            self.errorChecker.resetMessageError(validation_label: self.password_validation_label)
            break
        case self.register_confirm_password:
            self.errorChecker.resetMessageError(validation_label: self.confirm_password_validation_label)
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.register_email:
            let (valid, message) = self.validateFormat(textField)
            if let message = message, !valid {
                self.errorChecker.setMessageError(isHidden: valid, message: message, validation_label: self.email_validation_label)
            } else {
                self.register_password.becomeFirstResponder()
            }
            break
        case self.register_password:
            let (valid, message) = self.validateFormat(textField)
            if let message = message, !valid {
                self.errorChecker.setMessageError(isHidden: valid, message: message, validation_label: self.password_validation_label)
            } else {
                self.register_confirm_password.becomeFirstResponder()
            }
            break
        case self.register_confirm_password:
            let (valid, message) = self.validateFormat(textField)
            if let message = message, !valid {
                self.errorChecker.setMessageError(isHidden: valid, message: message, validation_label: self.confirm_password_validation_label)
            } else {
                self.checkRegister()
            }
            break
        default:
            break
        }
        
        return true
    }
    
    func validateFormat(_ textField: UITextField) -> (Bool, String?) {
        guard let text = textField.text else {
            return (false, nil)
        }
        
        if textField == self.register_password {
            return (text.count > 3, "The password is too short.")
        }
        
        return (text.count != 0, "This field cannot be empty.")
    }
    
    func validate(_ textField: UITextField) -> (Bool, String?) {
        let (valid, message) = self.validateFormat(textField)
        
        if !valid {
            return (valid, message)
        }
        
        return (true, nil)
    }
}
