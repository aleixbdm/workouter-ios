//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol LoginWithMDViewModelProtocol: class {
    var enviromentEmail: String? { get }
    var enviromentPassword: String? { get }
    func attemptLogin(email: String, password: String)
}

final class LoginWithMDViewModel: BaseViewModel, LoginWithMDViewModelProtocol {
    private let profileController: ProfileController

    required init(injector: Injector) {
        self.profileController = injector.profileController
        super.init(injector: injector)
    }
}

// MARK: Enviroment email
extension LoginWithMDViewModel {
    var enviromentEmail: String? {
        return EnvironmentVariables.Credentials.Email.value
    }
}

// MARK: Enviroment password
extension LoginWithMDViewModel {
    var enviromentPassword: String? {
        return EnvironmentVariables.Credentials.Password.value
    }
}

// MARK: Attempt Login
extension LoginWithMDViewModel {
    func attemptLogin(email: String, password: String) {
        let encryptedPassword = password.sha256.hexString
        self.profileController.login(email: email, password: encryptedPassword)
    }
}
