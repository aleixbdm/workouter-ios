//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit
import ReactiveSwift
import ReactiveCocoa

protocol LoginWithMDViewControllerProtocol: class {
    func checkLogin()
    func goToMain()
    func goToCreateAccount()
}

class LoginWithMDViewController: BaseViewController, ViewControllerType, LoginWithMDViewControllerProtocol {
    @IBOutlet weak var login_email: UITextField!
    @IBOutlet weak var login_password: UITextField!

    @IBOutlet weak var email_validation_label: UILabel!
    @IBOutlet weak var password_validation_label: UILabel!
    @IBOutlet weak var loginButton: MDButtonText!
    
    private var errorChecker = ErrorChecker()
    private var progressHandler = ProgressHandler()
    
    // MARK: View model
    var viewModel: LoginWithMDViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareViewModel()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
    
    // MARK: Actions
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        self.checkLogin()
    }
}

// MARK: Prepare UI
extension LoginWithMDViewController {
    func prepareUI() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        // Configure validation label
        self.email_validation_label.isHidden = true
        self.password_validation_label.isHidden = true

        // Configure textfields
        self.login_email.delegate = self
        self.login_password.delegate = self
        self.login_email.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.login_password.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)

        // Optional enviroment credentials
        self.login_email.text = self.viewModel.enviromentEmail
        self.login_password.text = self.viewModel.enviromentPassword

        // Hide keyboard when tap screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

// MARK: Prepare View Model
extension LoginWithMDViewController {
    func prepareViewModel() {
        self.viewModel.eventObserver.observeProfile { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let profile):
                Logger.log("profile id: \(profile.id ?? "Not found")")
                self?.progressHandler.hide()
                if profile.isProfileBasic {
                    self?.goToCreateAccount()
                } else {
                    self?.goToMain()
                }
            case .error(let error):
                Logger.log("error: \(error)")
                if case let .dbError(databaseError)? = (error as? MeetDayError),
                    case .emptyProfile = databaseError {
                    return
                }
                self?.progressHandler.hideWithError(error, origin: self, title: "Login error")
            }
        }
    }
}

// MARK: Check Login
extension LoginWithMDViewController {
    func checkLogin() {
        self.dismissKeyboard()

        guard let email = self.login_email.text,
            let password = self.login_password.text else {
            return
        }

        // Reset errors.
        self.errorChecker.resetMessageError(validation_label: self.email_validation_label)
        self.errorChecker.resetMessageError(validation_label: self.password_validation_label)

        var cancel: Bool = false
        var focusLabel: UITextField?

        let (email_valid, email_error_message) = self.validate(self.login_email)
        let (password_valid, password_error_message) = self.validate(self.login_password)

        if let message = email_error_message, !email_valid {
            focusLabel = focusLabel ?? self.login_email
            self.errorChecker.setMessageError(isHidden: email_valid, message: message, validation_label: self.email_validation_label)
            cancel = true
        }

        if let message = password_error_message, !password_valid {
            focusLabel = focusLabel ?? self.login_password
            self.errorChecker.setMessageError(isHidden: password_valid, message: message, validation_label: self.password_validation_label)
            cancel = true
        }

        if let focusLabel = focusLabel, cancel {
            focusLabel.becomeFirstResponder()
        } else {
            self.progressHandler.show(button: self.loginButton)
            self.viewModel.attemptLogin(email: email, password: password)
        }
    }
}

// MARK: Go to main
extension LoginWithMDViewController {
    func goToMain() {
        Navigator.shared.perform(self, segue: .loginWithMDToMainSegue)
    }
}

// MARK: Go to create account
extension LoginWithMDViewController {
    func goToCreateAccount() {
        Navigator.shared.perform(self, segue: .loginWithMDToCreateAccountSegue)
    }
}

// MARK: Textfield actions
extension LoginWithMDViewController: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case self.login_email:
            self.errorChecker.resetMessageError(validation_label: self.email_validation_label)
            break
        case self.login_password:
            self.errorChecker.resetMessageError(validation_label: self.password_validation_label)
            break
        default:
            break
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.login_email:
            let (valid, message) = self.validateFormat(textField)
            if let message = message, !valid {
                self.errorChecker.setMessageError(isHidden: valid, message: message, validation_label: self.email_validation_label)
            } else {
                self.login_password.becomeFirstResponder()
            }
            break
        case self.login_password:
            let (valid, message) = self.validateFormat(textField)
            if let message = message, !valid {
                self.errorChecker.setMessageError(isHidden: valid, message: message, validation_label: self.password_validation_label)
            } else {
                self.checkLogin()
            }
            break
        default:
            break
        }

        return true
    }

    func validateFormat(_ textField: UITextField) -> (Bool, String?) {
        guard let text = textField.text else {
            return (false, nil)
        }

        if textField == self.login_password {
            return (text.count > 3, "Your password is too short.")
        }

        return (text.count != 0, "This field cannot be empty.")
    }

    func validate(_ textField: UITextField) -> (Bool, String?) {
        guard let text = textField.text else {
            return (false, nil)
        }

        let (valid, message) = self.validateFormat(textField)

        if !valid {
            return (valid, message)
        }

        if textField == self.login_email {
            return (self.errorChecker.isEmailCorrect(email: text), "This email is incorrect.")
        }

        if textField == self.login_password {
            if let email = self.login_email.text {
                return (self.errorChecker.areCredentialsCorrect(email: email, password: text), "This password is incorrect.")
            }
        }

        return (true, nil)
    }
}
