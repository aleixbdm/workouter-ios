//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol RegisterViewModelProtocol: class {
    var enviromentEmail: String? { get }
    var enviromentPassword: String? { get }
    func attemptRegister(email: String, password: String)
}

final class RegisterViewModel: BaseViewModel, RegisterViewModelProtocol {
    private let profileController: ProfileController

    required init(injector: Injector) {
        self.profileController = injector.profileController
        super.init(injector: injector)
    }
}

// MARK: Enviroment email
extension RegisterViewModel {
    var enviromentEmail: String? {
        if let email = EnvironmentVariables.Credentials.Email.value, !email.isEmpty {
            return "\(arc4random_uniform(99))\(email)"
        }
        return nil
    }
}

// MARK: Enviroment password
extension RegisterViewModel {
    var enviromentPassword: String? {
        return EnvironmentVariables.Credentials.Password.value
    }
}

// MARK: Attempt register
extension RegisterViewModel {
    func attemptRegister(email: String, password: String) {
        let encryptedPassword = password.sha256.hexString
        self.profileController.register(email: email, password: encryptedPassword)
    }
}
