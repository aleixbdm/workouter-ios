//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol MainLoginViewModelProtocol: class {
    func attemptLoginWithFB(accessToken: String)
}

final class MainLoginViewModel: BaseViewModel, MainLoginViewModelProtocol {
    private let profileController: ProfileController

    required init(injector: Injector) {
        self.profileController = injector.profileController
        super.init(injector: injector)
    }
}

// MARK: Attempt Login with fb
extension MainLoginViewModel {
    func attemptLoginWithFB(accessToken: String) {
        self.profileController.loginWithFB(accessToken: accessToken)
    }
}
