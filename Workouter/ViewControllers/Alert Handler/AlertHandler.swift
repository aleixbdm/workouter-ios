//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class AlertHandler {
    class func showError(origin: UIViewController?, title: String, error: Error) {
        if let meetDayError = error as? MeetDayError {
            switch meetDayError {
            case .apiError(let message), .authError(let message):
                self.showAlert(origin: origin, title: title, message: message)
            case .dbError(let message):
                self.showAlert(origin: origin, title: title, message: message.rawValue)
            case .appError(let message):
                self.showAlert(origin: origin, title: title, message: message.rawValue)
            }
        } else {
            self.showAlert(origin: origin, title: title, message: error.localizedDescription)
        }
    }
    
    class func showAlert(origin: UIViewController?, title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    
        origin?.present(alert, animated: true)
    }
}
