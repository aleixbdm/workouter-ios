//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol MatchAlertDelegate : NSObjectProtocol{
    func userDidCancelAction(controller: MatchAlert)
    func userDidSelectGoChatsButton(controller: MatchAlert)
}

class MatchAlert: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var candidateImageView: UIImageView!
    
    weak var delegate: MatchAlertDelegate?
    
    private var tap : UITapGestureRecognizer?
    var candidate: Candidate?
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.modalPresentationCapturesStatusBarAppearance = true
        self.view.backgroundColor = UIColor.black(alpha: 0.5)
        self.prepareUI()
    }
    
    class func presentController(_ delegate: MatchAlertDelegate? = nil, candidate: Candidate) {
        let fromController : UIViewController? = UIApplication.topViewController()?.tabBarController ?? UIApplication.topViewController()
        let storyBoard = UIStoryboard (name: "Discover", bundle: nil)
        let alert : MatchAlert = storyBoard.instantiateViewController(withIdentifier: "MatchAlert") as! MatchAlert
        
        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overCurrentContext
        alert.delegate = delegate
        alert.candidate = candidate
        
        fromController?.present(alert, animated: true) {}
    }
    
    // MARK: Actions
    @IBAction func goChats(_ sender: Any) {
        if let currentDelegate = delegate {
            currentDelegate.userDidSelectGoChatsButton(controller: self)
        }
    }
    
    @objc private func closeAlert() {
        if let currentDelegate = delegate {
            currentDelegate.userDidCancelAction(controller: self)
        }
    }

    // MARK: Status bar style
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: Prepare UI
extension MatchAlert {
    func prepareUI() {
        self.tap = UITapGestureRecognizer(target: self, action: #selector(self.closeAlert))
        if let tap = self.tap {
            self.view.addGestureRecognizer(tap)
        }
        
        if let candidateName = candidate?.name {
            self.messageLabel.text = "You got a new match with \(candidateName)"
        }
        
        if let imageUrl = candidate?.imageUrls.first {
            self.candidateImageView.setImage(url: imageUrl, rounded: true)
        }
        
        self.containerView.addGestureRecognizer(UITapGestureRecognizer())
        self.containerView.backgroundColor = UIColor.white
    }
}
