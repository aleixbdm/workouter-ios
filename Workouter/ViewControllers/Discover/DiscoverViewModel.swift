//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol DiscoverViewModelProtocol: class {
    var dataSource: [Candidate] { get set }
    var selectedCandidate: Candidate? { get set }
    var didRunOutOfCandidates: Bool { get set }
    var lastEvaluatedCandidate: Candidate? { get }
    func attemptDownloadCandidates()
    func attemptEvaluate(index: Int, evaluation: EvaluationValue)
    func candidateForEvaluation(_ evaluation: Evaluation) -> Candidate?
}

final class DiscoverViewModel: BaseViewModel, DiscoverViewModelProtocol {
    private let candidateController: CandidateController
    var dataSource: [Candidate] = []
    var selectedCandidate: Candidate?
    var didRunOutOfCandidates: Bool = true
    private(set) var lastEvaluatedCandidate: Candidate?
    
    required init(injector: Injector) {
        self.candidateController = injector.candidateController
        super.init(injector: injector)
    }
    
    // MARK: Close
    override func close() {
        super.close()
        self.candidateController.discardCandidateProfile()
    }
}

// MARK: Attempt download candidates
extension DiscoverViewModel {
    func attemptDownloadCandidates() {
        self.candidateController.fetchCandidates()
    }
}

// MARK: Attempt evaluate
extension DiscoverViewModel {
    func attemptEvaluate(index: Int, evaluation: EvaluationValue) {
        guard self.dataSource.count > index else {
            // TODO: Handle error
            return
        }
        let candidate = self.dataSource[index]
        self.lastEvaluatedCandidate = candidate
        self.candidateController.evaluate(candidateId: candidate.id, evaluation: evaluation)
    }
}

// MARK: Candidate for evaluation
extension DiscoverViewModel {
    func candidateForEvaluation(_ evaluation: Evaluation) -> Candidate? {
        return self.dataSource.first(where: {$0.id == evaluation.senderUserId})
    }
}
