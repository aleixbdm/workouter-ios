//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit
import Koloda

protocol DiscoverViewControllerProtocol: class {
    func goCandidateProfile(_ candidate: Candidate)
}

class DiscoverViewController: BaseViewController, ViewControllerType, DiscoverViewControllerProtocol {
    @IBOutlet weak var kolodaView: KolodaView!
    
    // MARK: View model
    var viewModel: DiscoverViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        
        self.kolodaView.dataSource = self
        self.kolodaView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationTitle("Discovery")
        self.prepareViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.closeViewModel()
    }
}

// MARK: Prepare UI
extension DiscoverViewController {
    func prepareUI(){
       // Nothing to do
    }
}

// MARK: Prepare view model
extension DiscoverViewController {
    func prepareViewModel(){
        if self.viewModel.dataSource.isEmpty {
            self.viewModel.attemptDownloadCandidates()
        }
        self.viewModel.eventObserver.observeCandidates { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let candidates):
                Logger.log("candidates count: \(candidates.count)")
                self?.viewModel.dataSource = candidates
                self?.viewModel.didRunOutOfCandidates = candidates.isEmpty
                self?.kolodaView.resetCurrentCardIndex()
                self?.kolodaView.reloadData()
            case .error(let error):
                Logger.log("error: \(error)")
            }
        }
        self.viewModel.eventObserver.observeEvaluation { [weak self] (eventProperty) in
            switch eventProperty.event {
            case .success(let evaluation):
                Logger.log("evaluation value: \(evaluation.value.rawValue) for: \(evaluation.receiverUserId)")
                if case .Match = evaluation.value, let candidate = self?.viewModel.candidateForEvaluation(evaluation) {
                    MatchAlert.presentController(self, candidate: candidate)
                }
                if (self?.viewModel.lastEvaluatedCandidate?.id == evaluation.receiverUserId ||
                    self?.viewModel.lastEvaluatedCandidate?.id == evaluation.senderUserId) &&
                    self?.viewModel.didRunOutOfCandidates ?? false {
                    self?.viewModel.attemptDownloadCandidates()
                }
            case .error(let error):
                Logger.log("error: \(error)")
                self?.kolodaView.revertAction()
            }
        }
    }
}

// MARK: Go candidate profile
extension DiscoverViewController {
    func goCandidateProfile(_ candidate: Candidate) {
        self.viewModel.selectedCandidate = candidate
        Navigator.shared.perform(self, segue: .discoverToCandidateSegue)
    }
}

// MARK: KolodaViewDelegate
extension DiscoverViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.viewModel.didRunOutOfCandidates = true
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        self.goCandidateProfile(self.viewModel.dataSource[index])
    }
}

// MARK: KolodaViewDataSource
extension DiscoverViewController: KolodaViewDataSource {
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.viewModel.dataSource.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let candidateCard: CandidateCard = CandidateCard()
        let candidate: Candidate = self.viewModel.dataSource[index]
        candidateCard.setCandidate(candidate)
        return candidateCard
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return nil
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        let evaluation = EvaluationValue(swipeDirection: direction)
        self.viewModel.attemptEvaluate(index: index, evaluation: evaluation)
    }
}

// MARK: Koloda Actions
extension DiscoverViewController {
    @IBAction func dislikeButtonTapped() {
        self.kolodaView.swipe(.left)
    }
    
    @IBAction func likeButtonTapped() {
        self.kolodaView.swipe(.right)
    }
}

// MARK: MatchAlertDelegate
extension DiscoverViewController : MatchAlertDelegate {
    func userDidSelectGoChatsButton(controller: MatchAlert) {
        controller.dismiss(animated: true) {
            self.bottomTabBarController?.goChats()
        }
    }
    
    func userDidCancelAction(controller: MatchAlert) {
        controller.dismiss(animated: true, completion: nil)
    }
}
