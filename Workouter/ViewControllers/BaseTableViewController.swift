//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class BaseTableViewController: UITableViewController {}

// Mark: Auto rotate
extension BaseTableViewController {
    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

// MARK: Prepare segues
extension BaseTableViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        Navigator.shared.prepare(self, for: segue, sender: sender)
    }
}

// MARK: Navigation title
extension BaseTableViewController {
    func setNavigationTitle(_ title: String?) {
        self.navigationController?.navigationBar.topItem?.title = title
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
    }
}
