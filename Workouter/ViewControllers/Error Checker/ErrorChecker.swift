//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class ErrorChecker {
    func isEmailCorrect(email: String) -> Bool {
        return true
    }
    
    func areCredentialsCorrect(email: String, password: String) -> Bool {
        return true
    }
    
    func setMessageError(isHidden: Bool, message: String, validation_label: UILabel) {
        if validation_label.isHidden != isHidden {
            // Update Password Validation Label
            validation_label.text = message
            
            // Show Password Validation Label
            UIView.animate(withDuration: 0.25, animations: {
                validation_label.isHidden = isHidden
            })
        }
    }
    
    func resetMessageError(validation_label: UILabel){
        if !validation_label.isHidden {
            setMessageError(isHidden: true, message: "", validation_label: validation_label)
        }
    }
}
