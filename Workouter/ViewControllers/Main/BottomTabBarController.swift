//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol BottomTabBarControllerProtocol: class {
    func goChats()
}

class BottomTabBarController: BaseTabBarController, ViewControllerType, UITabBarControllerDelegate, BottomTabBarControllerProtocol {

    var discoverView: UIViewController!
    var chatView: UIViewController!
    var profileView: UIViewController!
    var settingsView: UIViewController!

    // MARK: View model
    var viewModel: BottomTabBarModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        
        self.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareViewModel()
    }
}

// MARK: Prepare UI
extension BottomTabBarController {
    func prepareUI() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.prepareTabBar()
        self.prepareChildControllers()
    }
    
    private func prepareTabBar() {
        self.tabBar.tintColor = UIColor.primary()
        self.tabBar.barTintColor = UIColor.backgroundButtonColor()
        
        self.tabBar.isTranslucent = false
    }
    
    private func prepareChildControllers() {
        // View initializations
        self.discoverView = Navigator.shared.instantiateViewController(self, withIdentifier: .DiscoverViewController)
        self.chatView = Navigator.shared.instantiateViewController(self, withIdentifier: .ChatListViewController)
        self.profileView = Navigator.shared.instantiateViewController(self, withIdentifier: .ProfileViewController)
        self.settingsView = Navigator.shared.instantiateViewController(self, withIdentifier: .SettingsViewController)
        
        self.viewControllers = [self.discoverView, self.chatView, self.profileView, self.settingsView]
        
        // Default selected view controller
        self.selectedViewController = self.discoverView
    }
}

// MARK: Prepare View Model
extension BottomTabBarController {
    func prepareViewModel() {
        self.viewModel.attemptDownloadChats()
    }
}

// MARK: Go chats
extension BottomTabBarController {
    func goChats() {
        self.selectedViewController = self.chatView
    }
}
