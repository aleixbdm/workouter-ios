//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol BottomTabBarModelProtocol: class {
    func attemptDownloadChats()
}

final class BottomTabBarModel: BaseViewModel, BottomTabBarModelProtocol {
    private let chatController: ChatController
    
    required init(injector: Injector) {
        self.chatController = injector.chatController
        super.init(injector: injector)
    }
}

// MARK: Attempt download chats
extension BottomTabBarModel {
    func attemptDownloadChats() {
        self.chatController.fetchChats()
        self.chatController.stopRefreshChats()
    }
}
