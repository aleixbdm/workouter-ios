//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol ViewControllerType: class {
    associatedtype VMType
    var viewModel: VMType! { get set }
    func prepareUI()
    func prepareViewModel()
    func closeViewModel()
}
extension ViewControllerType where VMType: ViewModelType {
    func closeViewModel() {
        self.viewModel.close()
    }
}
