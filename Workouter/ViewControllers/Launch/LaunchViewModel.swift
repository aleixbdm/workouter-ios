//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol LaunchViewModelProtocol: class {
    var isLoggedIn: Bool { get }
    var isAccountCreated: Bool { get }
}

final class LaunchViewModel: BaseViewModel, LaunchViewModelProtocol {
    private let profileController: ProfileController

    required init(injector: Injector) {
        self.profileController = injector.profileController
        super.init(injector: injector)
    }
}

// MARK: Is logged in
extension LaunchViewModel {
    var isLoggedIn: Bool {
        return self.profileController.auth != nil
    }
}

// MARK: Is profile created
extension LaunchViewModel {
    var isAccountCreated: Bool {
        return self.profileController.settings?.areSettingsConfigured ?? false
    }
}
