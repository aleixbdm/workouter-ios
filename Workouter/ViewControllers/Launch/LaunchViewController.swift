//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol LaunchViewControllerProtocol: class {
    func goToLogin()
    func goToMain()
    func goToCreateAccount()
}

class LaunchViewController: BaseViewController, ViewControllerType, LaunchViewControllerProtocol {

    // MARK: View model
    var viewModel: LaunchViewModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        // Want to prepare view model only when view did load
        self.prepareViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Check if we have logged in
        if self.viewModel.isLoggedIn {
            if self.viewModel.isAccountCreated {
                self.goToMain()
            } else {
                self.goToCreateAccount()
            }
        } else {
            self.goToLogin()
        }
    }

    // MARK: Status bar style
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: Prepare UI
extension LaunchViewController {
    func prepareUI() {
        // Nothing to do
    }
}

// MARK: Prepare View Model
extension LaunchViewController {
    func prepareViewModel() {
        let viewModelType : ProvisionType = .Launch(injector: Injector())
        ViewModelProvider.shared.provide(self, type: viewModelType)
    }
}

// MARK: Protocol Methods
extension LaunchViewController {
    func goToLogin() {
        Navigator.shared.perform(self, segue: .launchToLoginSegue)
    }
    
    func goToMain() {
        Navigator.shared.perform(self, segue: .launchToMainSegue)
    }

    func goToCreateAccount() {
        Navigator.shared.perform(self, segue: .launchToCreateAccountSegue)
    }
}
