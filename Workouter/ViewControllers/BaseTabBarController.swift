//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

class BaseTabBarController: UITabBarController {}

// Mark: Auto rotate
extension BaseTabBarController {
    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
