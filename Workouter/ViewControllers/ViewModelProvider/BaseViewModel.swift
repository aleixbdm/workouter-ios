//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto

protocol BaseViewModelProtocol: class {
    func provide(_ provisionTypeConstructor: (Injector) -> ProvisionType, to baseViewController: BaseViewController)
    func provide(_ provisionTypeConstructor: (Injector) -> ProvisionType, to baseTabBarController: BottomTabBarController)
    func provide(_ provisionTypeConstructor: (Injector) -> ProvisionType, to baseTableViewController: BaseTableViewController)
    func provide(_ provisionTypeConstructor: (Injector, Candidate) -> ProvisionType, candidate: Candidate, to baseViewController: BaseViewController)
    func provide(_ provisionTypeConstructor: (Injector, Chat) -> ProvisionType, chat: Chat, to baseChatViewController: BaseChatViewController)
}

class BaseViewModel: ViewModelType {
    private let injector: Injector
    private(set) var eventObserver: EventObserver
    
    required init(injector: Injector) {
        self.injector = injector
        self.eventObserver = injector.eventObserver
    }
    
    func close() {
        self.eventObserver.removeObservers()
    }
}

// MARK: Provide
extension BaseViewModel {
    func provide(_ provisionTypeConstructor: (Injector) -> ProvisionType, to baseViewController: BaseViewController) {
        ViewModelProvider.shared.provide(baseViewController, type: provisionTypeConstructor(self.injector))
    }
    
    func provide(_ provisionTypeConstructor: (Injector) -> ProvisionType, to baseTabBarController: BottomTabBarController) {
        ViewModelProvider.shared.provide(baseTabBarController, type: provisionTypeConstructor(self.injector))
    }
    
    func provide(_ provisionTypeConstructor: (Injector) -> ProvisionType, to baseTableViewController: BaseTableViewController) {
        ViewModelProvider.shared.provide(baseTableViewController, type: provisionTypeConstructor(self.injector))
    }
    
    func provide(_ provisionTypeConstructor: (Injector, Candidate) -> ProvisionType, candidate: Candidate, to baseViewController: BaseViewController) {
        ViewModelProvider.shared.provide(baseViewController, type: provisionTypeConstructor(self.injector, candidate))
    }
    
    func provide(_ provisionTypeConstructor: (Injector, Chat) -> ProvisionType, chat: Chat, to baseChatViewController: BaseChatViewController) {
        ViewModelProvider.shared.provide(baseChatViewController, type: provisionTypeConstructor(self.injector, chat))
    }
}
