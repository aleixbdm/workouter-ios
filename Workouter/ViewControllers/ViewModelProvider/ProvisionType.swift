//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

enum ProvisionType {
    case Launch(injector: Injector)
    case MainLogin(injector: Injector)
    case LoginWithMD(injector: Injector)
    case Register(injector: Injector)
    case CreateAccount(injector: Injector)
    case BottomTabBar(injector: Injector)
    case Discover(injector: Injector)
    case ChatList(injector: Injector)
    case Chat(injector: Injector, chat: Chat)
    case Profile(injector: Injector)
    case CandidateProfile(injector: Injector, candidate: Candidate)
    case Settings(injector: Injector)
    
    var instance: ViewModelType {
        switch self {
        case let .Launch(injector: injector):
            return LaunchViewModel(injector: injector)
        case let .MainLogin(injector: injector):
            return MainLoginViewModel(injector: injector)
        case let .LoginWithMD(injector: injector):
            return LoginWithMDViewModel(injector: injector)
        case let .Register(injector: injector):
            return RegisterViewModel(injector: injector)
        case let .CreateAccount(injector: injector):
            return CreateAccountViewModel(injector: injector)
        case let .BottomTabBar(injector: injector):
            return BottomTabBarModel(injector: injector)
        case let .Discover(injector: injector):
            return DiscoverViewModel(injector: injector)
        case let .ChatList(injector: injector):
            return ChatListViewModel(injector: injector)
        case let .Chat(injector: injector, chat: c):
            return ChatViewModel(injector: injector, chat: c)
        case let .Profile(injector: injector):
            return ProfileViewModel(injector: injector)
        case let .CandidateProfile(injector: injector, candidate: c):
            return ProfileViewModel(injector: injector, candidate: c)
        case let .Settings(injector: injector):
            return SettingsViewModel(injector: injector)
        }
    }
}
