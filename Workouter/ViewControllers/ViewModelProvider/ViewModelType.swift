//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

protocol ViewModelType {
    var eventObserver: EventObserver { get }
    init(injector: Injector)
    func close()
}

extension ViewModelType {
    func close() {
        self.eventObserver.removeObservers()
    }
}
