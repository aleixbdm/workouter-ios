//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Chatto

protocol ViewModelProviderProtocol: class {
    func provide(_ baseViewController: BaseViewController, type: ProvisionType)
    func provide(_ baseTabBarController: BaseTabBarController, type: ProvisionType)
    func provide(_ baseTableViewController: BaseTableViewController, type: ProvisionType)
    func provide(_ baseChatViewController: BaseChatViewController, type: ProvisionType)
}

class ViewModelProvider: ViewModelProviderProtocol {
    static let shared = ViewModelProvider()
}

// MARK: Error Log
extension ViewModelProvider {
    private func logProvideViewModelError(_ type: ProvisionType) {
        Logger.log("could not provide \(type)")
    }
}

// MARK: Provide base view controller
extension ViewModelProvider {
    func provide(_ baseViewController: BaseViewController, type: ProvisionType){
        switch type {
        case .Launch:
            guard let viewController = baseViewController as? LaunchViewController,
                let viewModel = type.instance as? LaunchViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        case .MainLogin:
            guard let viewController = baseViewController as? MainLoginViewController,
                let viewModel = type.instance as? MainLoginViewModel else {
                return
            }
            viewController.viewModel = viewModel
        case .LoginWithMD:
            guard let viewController = baseViewController as? LoginWithMDViewController,
                let viewModel = type.instance as? LoginWithMDViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        case .Register:
            guard let viewController = baseViewController as? RegisterViewController,
                let viewModel = type.instance as? RegisterViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        case .CreateAccount:
            guard let viewController = baseViewController as? CreateAccountViewController,
                let viewModel = type.instance as? CreateAccountViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        case .Discover:
            guard let viewController = baseViewController as? DiscoverViewController,
                let viewModel = type.instance as? DiscoverViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        case .Profile, .CandidateProfile:
            guard let viewController = baseViewController as? ProfileViewController,
                let viewModel = type.instance as? ProfileViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        case .Settings:
            guard let viewController = baseViewController as? SettingsViewController,
                let viewModel = type.instance as? SettingsViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        default:
            self.logProvideViewModelError(type)
        }
    }
}

// MARK: Provide base tab bar controller
extension ViewModelProvider {
    func provide(_ baseTabBarController: BaseTabBarController, type: ProvisionType) {
        switch type {
        case .BottomTabBar:
            guard let tabBarController = baseTabBarController as? BottomTabBarController,
                let tabBarModel = type.instance as? BottomTabBarModel else {
                    return
            }
            tabBarController.viewModel = tabBarModel
        default:
            self.logProvideViewModelError(type)
        }
    }
}

// MARK: Provide base table view controller
extension ViewModelProvider {
    func provide(_ baseTableViewController: BaseTableViewController, type: ProvisionType) {
        switch type {
        case .ChatList:
            guard let viewController = baseTableViewController as? ChatListViewController,
                let viewModel = type.instance as? ChatListViewModel else {
                    return
            }
            viewController.viewModel = viewModel
        default:
            self.logProvideViewModelError(type)
        }
    }
}

// MARK: Provide base chat view controller
extension ViewModelProvider {
    func provide(_ baseChatViewController: BaseChatViewController, type: ProvisionType) {
        switch type {
        case .Chat:
            guard let viewController = baseChatViewController as? ChatViewController,
                let viewModel = type.instance as? ChatViewModel,
                let userId = viewModel.userId else {
                    return
            }
            viewController.viewModel = viewModel
            let dataSource = ChatDataSource(userId: userId, pageSize: ChatPageSize)
            viewController.dataSource = dataSource
        default:
            self.logProvideViewModelError(type)
        }
    }
}
