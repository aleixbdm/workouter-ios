//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

extension URL {
    func with(networkParameters: NetworkParameters?) -> URL? {
        guard let networkParameters = networkParameters else {
            return self
        }
        var queryUrl = self
        guard var components = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            return nil
        }
        let queryParams = networkParameters[.query]
        for queryParam in queryParams {
            components.queryItems = [
                URLQueryItem(name: queryParam.key, value: String(describing: queryParam.associatedValue))
            ]
        }
        do {
            queryUrl = try components.asURL()
        } catch {
            return nil
        }
        return queryUrl
    }
}
