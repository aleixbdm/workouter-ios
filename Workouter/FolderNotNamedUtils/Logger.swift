//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

struct Logger {
    static func log(_ msg: String, line: Int = #line, fileName: String = #file, funcName: String = #function) {
        let fname = (fileName as NSString).lastPathComponent
        NSLog("[Logger] \(fname):\(funcName):\(line): \(msg)")
    }
}
