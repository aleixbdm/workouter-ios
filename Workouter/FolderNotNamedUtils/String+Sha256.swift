//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

extension Data {
    var hexString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
    
    var sha256: Data {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        self.withUnsafeBytes({
            _ = CC_SHA256($0, CC_LONG(self.count), &digest)
        })
        return Data(bytes: digest)
    }
}

extension String {
    var sha256: Data {
        return self.data(using: .utf8)!.sha256
    }
}
