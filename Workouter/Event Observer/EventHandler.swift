//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ReactiveSwift
import ReactiveCocoa

class EventHandler {
    private var disposables: [Disposable] = []
    
    func observe<E, T: EventProperty<E>>(on: MutableProperty<T>,_ action: @escaping (T) -> Void) {
        let disposable = on.producer.observe(on: UIScheduler())
            .filter({ (eventProperty) -> Bool in
                return !eventProperty.read
            })
            .startWithValues(action)
        self.disposables.append(disposable)
    }
    
    func dispose() {
        for disposable in self.disposables {
            disposable.dispose()
        }
        self.disposables.removeAll()
    }
}
