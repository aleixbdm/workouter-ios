//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol EventObserverProtocol: class {
    init(modelController: ModelController)
    func observeProfile(_ action: @escaping (EventProperty<Profile>) -> Void)
    func observeCandidates(_ action: @escaping (EventProperty<[Candidate]>) -> Void)
    func observeCandidateProfile(_ action: @escaping (EventProperty<Profile>) -> Void)
    func observeEvaluation(_ action: @escaping (EventProperty<Evaluation>) -> Void)
    func observeChats(_ action: @escaping (EventProperty<[Chat]>) -> Void)
    func observeChatMessages(_ action: @escaping (EventProperty<[MessageChat]>) -> Void)
    func observeChatMessageSent(_ action: @escaping (EventProperty<MessageChat>) -> Void)
    func removeObservers()
}

final class EventObserver: EventObserverProtocol {
    private let modelController: ModelController
    private let eventHandler: EventHandler

    init(modelController: ModelController) {
        self.modelController = modelController
        self.eventHandler = EventHandler()
    }
}

// MARK: Observe profile
extension EventObserver {
    func observeProfile(_ action: @escaping (EventProperty<Profile>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.profileRepository.profileProperty, action)
    }
}

// MARK: Observe candidates
extension EventObserver {
    func observeCandidates(_ action: @escaping (EventProperty<[Candidate]>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.candidateRepository.candidatesProperty, action)
    }
}

// MARK: Observe candidate profile
extension EventObserver {
    func observeCandidateProfile(_ action: @escaping (EventProperty<Profile>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.candidateRepository.candidateProfileProperty, action)
    }
}

// MARK: Observe evaluation
extension EventObserver {
    func observeEvaluation(_ action: @escaping (EventProperty<Evaluation>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.candidateRepository.evaluationProperty, action)
    }
}

// MARK: Observe chats
extension EventObserver {
    func observeChats(_ action: @escaping (EventProperty<[Chat]>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.chatRepository.chatsProperty, action)
    }
}

// MARK: Observe chat messages
extension EventObserver {
    func observeChatMessages(_ action: @escaping (EventProperty<[MessageChat]>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.chatRepository.chatMessagesProperty, action)
    }
}

// MARK: Observe chat message sent
extension EventObserver {
    func observeChatMessageSent(_ action: @escaping (EventProperty<MessageChat>) -> Void) {
        self.eventHandler.observe(on: self.modelController.repository.chatRepository.chatMessageSentProperty, action)
    }
}

// MARK: Remove observers
extension EventObserver {
    func removeObservers() {
        self.eventHandler.dispose()
    }
}

