//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import UIKit

extension UIColor {
    class func primary() -> UIColor {
        return UIColor(red: 181.0 / 255.0, green: 71.0 / 255.0, blue: 63.0 / 255.0, alpha: 255.0 / 255.0)
    }
    
    class func primaryLight() -> UIColor {
        return UIColor(red: 239.0 / 255.0, green: 154.0 / 255.0, blue: 154.0 / 255.0, alpha: 255.0 / 255.0)
    }
    
    class func backgroundButtonColor() -> UIColor {
        return white(230.0, alpha: 126.0 / 255.0)
    }
    
    class func containerBackground() -> UIColor {
        return UIColor(red: 220.0 / 255.0, green: 216.0 / 255.0, blue: 231.0 / 255.0, alpha: 1)
    }
    
    class func chatBackground() -> UIColor {
        return white(245.0, alpha: 1)
    }
    
    class func white(_ tone: CGFloat = 255, alpha: CGFloat = 1.0) -> UIColor{ // tone must be between 0 - 255
        return UIColor(red: tone / 255, green: tone / 255, blue: tone / 255, alpha: alpha)
    }
    
    class func black(alpha: CGFloat = 1)  -> UIColor {
        return white(0, alpha: alpha)
    }
}
