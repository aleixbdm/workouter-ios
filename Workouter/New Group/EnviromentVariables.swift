//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

public struct EnvironmentVariables {
    public struct Credentials {
        public static let Email: NamedProperty<String> = EnvironmentProperty<String>("workouter.credentials.email", default: "")
        public static let Password: NamedProperty<String> = EnvironmentProperty<String>("workouter.credentials.password", default: "")
    }
}

public class EnvironmentProperty<T: LosslessStringConvertible>: NamedProperty<T> {
    private let store: ProcessInfo

    public convenience override init(_ name: String, default value: T? = nil) {
        self.init(name, store: ProcessInfo.processInfo, default: value)
    }

    public init(_ name: String, store: ProcessInfo, default value: T? = nil) {
        self.store = store
        super.init(name, default: value)
    }

    public override var value: T? {
        get {
            if let variable: String = self.store.environment[self.name] {
                return T(variable) ?? self.`default`
            }
            return self.`default`
        }
        set {
            fatalError("[EnvironmentProperty] Variable '\(self.name)' cannot be set.")
        }
    }
}

public class NamedProperty<T>: Property {
    internal let `default`: T?

    public let name: String
    public var value: T?

    public init(_ name: String, default value: T? = nil) {
        self.default = value
        self.name = name
    }
}

public protocol Property {
    associatedtype ValueType

    var name: String { get }
    var value: ValueType? { get }
}
