//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

let DefaultString = ""
let DefaultInt = 0
let DefaultArrayString = [String]()

let BaseUrl = "http://localhost:3000/api/v1/"
// let BaseUrl = "https://meetday-mdpa-aleixpellisa.azurewebsites.net/api/v1/"
let MinAge = 18
let MaxAge = 65
let NumberOfCards = 5
let DefaultDiscoverLimit = 5
let DefaultChatMessagesLimit = 5
let ChatPageSize: Int = 50
let RefreshInterval = 3.0
