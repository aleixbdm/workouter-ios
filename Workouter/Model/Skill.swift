//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift

enum SkillCategory: String {
    case unknown
    case sports
    case geek
    case dance
    case travel
    case pets
    case green
}

class Skill: Object {
    @objc dynamic private var category: String?
    let value = RealmOptional<Int>()
    
    override static func primaryKey() -> String? {
        return "category"
    }
    
    static func model(skillSchema: SkillSchema) -> Skill {
        let skill = Skill()
        skill.category = skillSchema.category
        skill.value.value = skillSchema.value
        return skill
    }
}

extension Skill {
    var categoryType: SkillCategory {
        return SkillCategory(rawValue: self.category ?? "") ?? .unknown
    }
}
