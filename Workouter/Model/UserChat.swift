//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift

class UserChat: Object {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var firstName: String?
    @objc dynamic var imageUrl: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func model(userChatSchema: UserChatSchema) -> UserChat {
        let userChat = UserChat()
        userChat.id = userChatSchema.id
        userChat.name = userChatSchema.name
        userChat.firstName = userChatSchema.firstName
        userChat.imageUrl = userChatSchema.imageUrl
        return userChat
    }
}

