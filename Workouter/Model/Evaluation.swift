//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Koloda

enum EvaluationValue: String {
    case unknown
    case Like
    case Dislike
    case Match
}

extension EvaluationValue {
    init(swipeDirection: SwipeResultDirection) {
        switch swipeDirection {
        case .right, .bottomRight, .topRight:
            self = .Like
        default:
            self = .Dislike
        }
    }
}

struct Evaluation {
    var id: String
    var senderUserId: String
    var receiverUserId: String
    var value: EvaluationValue
    var chatId: String?
    
    static func model(evaluationSchema: EvaluationSchema) -> Evaluation {
        return Evaluation(id: evaluationSchema.id, senderUserId: evaluationSchema.senderUserId, receiverUserId: evaluationSchema.receiverUserId, value: EvaluationValue(rawValue: evaluationSchema.value) ?? .unknown, chatId: evaluationSchema.chatId)
    }
}
