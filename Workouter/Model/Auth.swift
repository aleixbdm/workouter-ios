//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

struct Auth: Decodable {
    let accessToken: String
    let userId: String
}
