//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift

class MessageChat: Object {
    @objc dynamic var id: String?
    @objc dynamic var chatId: String?
    @objc dynamic var senderUserId: String?
    @objc dynamic var messageText: String?
    @objc dynamic var createdAt: Date?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func model(messageChatSchema: MessageChatSchema) -> MessageChat {
        let messageChat = MessageChat()
        messageChat.id = messageChatSchema.id
        messageChat.chatId = messageChatSchema.chatId
        messageChat.senderUserId = messageChatSchema.senderUserId
        messageChat.messageText = messageChatSchema.messageText
        messageChat.createdAt = Date(milliseconds: messageChatSchema.createdAt)
        return messageChat
    }
}
