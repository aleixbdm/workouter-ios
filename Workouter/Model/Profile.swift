//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift

enum Gender: String {
    case unknown
    case male = "Male"
    case female = "Female"
}

class Profile: Object {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var firstName: String?
    let imageUrls = List<String>()
    let age = RealmOptional<Int>()
    @objc dynamic private var gender: String?
    @objc dynamic var generalInformation: String?
    @objc dynamic var currentJob: String?
    @objc dynamic var studies: String?
    @objc dynamic var favouriteSong: String?
    let skills = List<Skill>()
    @objc dynamic var settings: Settings?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func model(profileSchema: ProfileSchema) -> Profile {
        let profile = Profile()
        profile.id = profileSchema.id
        profile.name = profileSchema.name
        profile.firstName = profileSchema.firstName
        let imageUrls: [String] = profileSchema.imageUrls ?? []
        profile.imageUrls.append(objectsIn: imageUrls)
        profile.age.value = profileSchema.age
        profile.gender = profileSchema.gender
        profile.generalInformation = profileSchema.generalInformation
        profile.currentJob = profileSchema.currentJob
        profile.studies = profileSchema.studies
        profile.favouriteSong = profileSchema.favouriteSong
        let skills: [Skill] = profileSchema.skills?.map({Skill.model(skillSchema: $0)}) ?? []
        profile.skills.append(objectsIn: skills)
        if let settingsSchema = profileSchema.settings {
            profile.settings = Settings.model(profileId: profileSchema.id, settingsSchema: settingsSchema)
        }
        return profile
    }
}

extension Profile {
    var genderType: Gender {
        return Gender(rawValue: self.gender ?? "") ?? .unknown
    }
}

extension Profile {
    var isProfileBasic: Bool {
        return !(self.settings?.areSettingsConfigured ?? false)
    }
}
