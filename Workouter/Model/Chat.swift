//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift

class Chat: Object {
    @objc dynamic var id: String?
    @objc dynamic var candidateUser: UserChat?
    @objc dynamic var lastMessage: MessageChat?
    @objc dynamic var updatedAt: Date?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func model(chatSchema: ChatSchema) -> Chat {
        let chat = Chat()
        chat.id = chatSchema.id
        chat.candidateUser = UserChat.model(userChatSchema: chatSchema.candidateUser)
        if let lastMessage = chatSchema.lastMessage {
            chat.lastMessage = MessageChat.model(messageChatSchema: lastMessage)
        }
        chat.updatedAt = Date(milliseconds: chatSchema.updatedAt)
        return chat
    }
}

