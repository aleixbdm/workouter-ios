//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

struct Candidate {
    let id: String
    let name: String
    let imageUrls: [String]
    let age: Int
    let skills: [Skill]
    let city: String

    static func model(candidateSchema: CandidateSchema) -> Candidate {
        let skills: [Skill] = candidateSchema.skills?.map({Skill.model(skillSchema: $0)}) ?? []
        return Candidate(id: candidateSchema.id, name: candidateSchema.name, imageUrls: candidateSchema.imageUrls, age: candidateSchema.age, skills: skills, city: candidateSchema.city)
    }
}
