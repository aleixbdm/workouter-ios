//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

enum MeetDayError: Error {
    case appError(AppError)
    case dbError(DatabaseError)
    case apiError(message: String?)
    case authError(message: String?)
}

enum AppError: String {
    case createAccount
}

enum DatabaseError: String {
    case emptyProfile
    case emptyCandidates
    case emptyCandidateProfile
    case emptyEvaluation
    case emptyChats
    case emptyChatMessages
    case emptyChatMessageSent
}
