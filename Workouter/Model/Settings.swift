//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift

enum Preference: String {
    case unknown
    case male = "Male"
    case female = "Female"
    case all = "All"
}

class Settings: Object {
    @objc dynamic private var profileId: String?
    let visible = RealmOptional<Bool>()
    let yearRangeMax = RealmOptional<Int>()
    let yearRangeMin = RealmOptional<Int>()
    let searchRange = RealmOptional<Int>()
    @objc dynamic var city: String?
    @objc dynamic private var preference: String?
    
    override static func primaryKey() -> String? {
        return "profileId"
    }
    
    static func model(profileId: String, settingsSchema: SettingsSchema) -> Settings {
        let settings = Settings()
        settings.profileId = profileId
        settings.visible.value = settingsSchema.visible
        settings.yearRangeMax.value = settingsSchema.yearRangeMax
        settings.yearRangeMin.value = settingsSchema.yearRangeMin
        settings.searchRange.value = settingsSchema.searchRange
        settings.city = settingsSchema.city
        settings.preference = settingsSchema.preference
        return settings
    }

    static func model(profileId: String, visible: Bool?, yearRangeMax: Int?, yearRangeMin: Int?, searchRange: Int?, city: String?, preference: String?) -> Settings {
        let settings = Settings()
        settings.profileId = profileId
        settings.visible.value = visible
        settings.yearRangeMax.value = yearRangeMax
        settings.yearRangeMin.value = yearRangeMin
        settings.searchRange.value = searchRange
        settings.city = city
        settings.preference = preference
        return settings
    }
}

extension Settings {
    var preferenceType: Preference {
        return Preference(rawValue: self.preference ?? "") ?? .unknown
    }
}

extension Settings {
    var areSettingsConfigured: Bool {
        return self.preferenceType != .unknown
    }
}
