//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol InjectorProtocol: class {
    var eventObserver: EventObserver { get }
    var profileController: ProfileController { get }
    var candidateController: CandidateController { get }
    var chatController: ChatController { get }
}

class Injector: InjectorProtocol {
    private(set) var profileController: ProfileController
    private(set) var candidateController: CandidateController
    private(set) var chatController: ChatController
    private let modelController: ModelController
    
    init() {
        self.modelController = ModelController()
        let networkController = NetworkController()
        self.profileController = ProfileController(modelController: modelController, networkController: networkController)
        self.candidateController = CandidateController(modelController: modelController, networkController: networkController)
        self.chatController = ChatController(modelController: modelController, networkController: networkController)
    }
}

// MARK: Event observer
extension Injector {
    var eventObserver: EventObserver {
        return EventObserver(modelController: modelController)
    }
}

