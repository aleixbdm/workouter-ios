//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

class EventProperty<T> {
    private var _event: Event<T>
    private(set) var read: Bool

    init(event: Event<T>) {
        self._event = event
        self.read = false
    }

    var event: Event<T> {
        self.read = true
        return self._event
    }
}
