//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

enum Event<T> {
    case success(T)
    case error(Error)
}
