//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import Foundation

protocol UserDefaultsRepositoryProtocol: class {
    var auth: Auth? { get }
    var settings: Settings? { get }
    func saveAuth(_ authSchema: AuthSchema)
    func saveSettings(_ settingsSchema: SettingsSchema)
    func deleteAll()
}

class UserDefaultsRepository: UserDefaultsRepositoryProtocol {
    private let userDefaults: UserDefaults
    private(set) var auth: Auth?
    private(set) var settings: Settings?
    
    init() {
        self.userDefaults = UserDefaults.standard
        self.initAuth()
        self.initSettings()
    }
    
    private func initAuth() {
        guard let accessToken = self.userDefaults.string(forKey: AuthSchema.Keys.AccessToken),
            let userId = self.userDefaults.string(forKey: AuthSchema.Keys.UserId) else {
            return
        }
        self.auth = Auth(accessToken: accessToken, userId: userId)
    }
    
    private func initSettings() {
        guard let profileId = self.userDefaults.string(forKey: AuthSchema.Keys.UserId) else {
                return
        }
        let visible = self.userDefaults.bool(forKey: SettingsSchema.Keys.Visible)
        let yearRangeMax = self.userDefaults.integer(forKey: SettingsSchema.Keys.YearRangeMax)
        let yearRangeMin = self.userDefaults.integer(forKey: SettingsSchema.Keys.YearRangeMin)
        let searchRange = self.userDefaults.integer(forKey: SettingsSchema.Keys.SearchRange)
        let city = self.userDefaults.string(forKey: SettingsSchema.Keys.City)
        let preference = self.userDefaults.string(forKey: SettingsSchema.Keys.Preference)
        self.settings = Settings.model(profileId: profileId, visible: visible, yearRangeMax: yearRangeMax, yearRangeMin: yearRangeMin, searchRange: searchRange, city: city, preference: preference)
    }

    // MARK: Save auth
    func saveAuth(_ authSchema: AuthSchema) {
        self.userDefaults.set(authSchema.accessToken, forKey: AuthSchema.Keys.AccessToken)
        self.userDefaults.set(authSchema.userId, forKey: AuthSchema.Keys.UserId)
        self.auth = Auth(accessToken: authSchema.accessToken, userId: authSchema.userId)
    }

    // MARK: Save settings
    func saveSettings(_ settingsSchema: SettingsSchema) {
        guard let profileId = self.auth?.userId else {
            return
        }
        self.userDefaults.set(settingsSchema.visible, forKey: SettingsSchema.Keys.Visible)
        self.userDefaults.set(settingsSchema.yearRangeMax, forKey: SettingsSchema.Keys.YearRangeMax)
        self.userDefaults.set(settingsSchema.yearRangeMin, forKey: SettingsSchema.Keys.YearRangeMin)
        self.userDefaults.set(settingsSchema.searchRange, forKey: SettingsSchema.Keys.SearchRange)
        self.userDefaults.set(settingsSchema.city, forKey: SettingsSchema.Keys.City)
        self.userDefaults.set(settingsSchema.preference, forKey: SettingsSchema.Keys.Preference)
        self.settings = Settings.model(profileId: profileId, settingsSchema: settingsSchema)
    }
    
    // MARK: Delete all
    func deleteAll() {
        self.clearAuth()
        self.clearSettings()
    }
}

// MARK: Clear methods
extension UserDefaultsRepository {
    private func clearAuth() {
        self.userDefaults.removeObject(forKey: AuthSchema.Keys.AccessToken)
        self.userDefaults.removeObject(forKey: AuthSchema.Keys.UserId)
        self.auth = nil
    }

    private func clearSettings() {
        self.userDefaults.removeObject(forKey: SettingsSchema.Keys.Visible)
        self.userDefaults.removeObject(forKey: SettingsSchema.Keys.YearRangeMax)
        self.userDefaults.removeObject(forKey: SettingsSchema.Keys.YearRangeMin)
        self.userDefaults.removeObject(forKey: SettingsSchema.Keys.SearchRange)
        self.userDefaults.removeObject(forKey: SettingsSchema.Keys.City)
        self.userDefaults.removeObject(forKey: SettingsSchema.Keys.Preference)
        self.settings = nil
    }
}
