//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift
import ReactiveSwift

protocol ChatRepositoryProtocol: class {
    var chatsProperty: MutableProperty<EventProperty<[Chat]>> { get }
    var chatMessagesProperty: MutableProperty<EventProperty<[MessageChat]>> { get }
    var chatMessageSentProperty: MutableProperty<EventProperty<MessageChat>> { get }
    init(realm: Realm)
    func saveChats(_ chatSchemas: [ChatSchema])
    func saveChatsError(_ error: Error)
    func loadChatMessages(chatId: String)
    func saveChatMessages(_ messageChatSchemas: [MessageChatSchema])
    func saveChatMessagesError(_ error: Error)
    func unloadChatMessages()
    func saveChatMessageSent(_ messageChatSchema: MessageChatSchema)
    func saveChatMessageSentError(_ error: Error)
    func restartEvents()
}

final class ChatRepository: ChatRepositoryProtocol {
    private let realm: Realm
    private var chatsEvent: Event<[Chat]> {
        didSet {
            self.chatsProperty.value = EventProperty(event: self.chatsEvent)
        }
    }
    private var chatMessagesEvent: Event<[MessageChat]> {
        didSet {
            self.chatMessagesProperty.value = EventProperty(event: self.chatMessagesEvent)
        }
    }
    private var chatMessageSentEvent: Event<MessageChat> {
        didSet {
            self.chatMessageSentProperty.value = EventProperty(event: self.chatMessageSentEvent)
        }
    }
    private(set) var chatsProperty: MutableProperty<EventProperty<[Chat]>>
    private(set) var chatMessagesProperty: MutableProperty<EventProperty<[MessageChat]>>
    private(set) var chatMessageSentProperty: MutableProperty<EventProperty<MessageChat>>
    
    init(realm: Realm) {
        self.realm = realm
        self.chatsEvent = Event.error(MeetDayError.dbError(.emptyChats))
        self.chatsProperty = MutableProperty(EventProperty(event: self.chatsEvent))

        self.chatMessagesEvent = Event.error(MeetDayError.dbError(.emptyChatMessages))
        self.chatMessagesProperty = MutableProperty(EventProperty(event: self.chatMessagesEvent))
        
        self.chatMessageSentEvent = Event.error(MeetDayError.dbError(.emptyChatMessageSent))
        self.chatMessageSentProperty = MutableProperty(EventProperty(event: self.chatMessageSentEvent))
        
        self.loadChats()
    }
}

// MARK: Save chats
extension ChatRepository {
    func saveChats(_ chatSchemas: [ChatSchema]) {
        let chats: [Chat] = chatSchemas.map{Chat.model(chatSchema:$0)}
        
        try? self.realm.write {
            self.realm.add(chats, update: true)
            self.chatsEvent = Event.success(chats)
        }
    }
}

// MARK: Save chats error
extension ChatRepository {
    func saveChatsError(_ error: Error) {
        self.chatsEvent = Event.error(error)
    }
}

// MARK: Load chat messages
extension ChatRepository {
    func loadChatMessages(chatId: String) {
        if case let .success(event) = self.chatMessagesEvent,
            event.first?.chatId == chatId {
            return
        }
        let chatMessageSendId: String?
        if case let .success(event) = self.chatMessageSentEvent,
            let id = event.id {
            chatMessageSendId = id
        } else {
            chatMessageSendId = nil
        }
        let predicate = NSPredicate(format: "chatId == %@", chatId)
        let chatMessages: [MessageChat] = self.realm.objects(MessageChat.self).filter(predicate).map({$0}).filter({$0.id != chatMessageSendId})
        if !chatMessages.isEmpty {
            self.chatMessagesEvent = Event.success(chatMessages)
        } else {
            self.chatMessagesEvent = Event.error(MeetDayError.dbError(.emptyChatMessages))
        }
    }
}

// MARK: Save chat messages
extension ChatRepository {
    func saveChatMessages(_ messageChatSchemas: [MessageChatSchema]) {
        guard let chatId = messageChatSchemas.first?.chatId else { return }
        let predicate = NSPredicate(format: "id == %@", chatId)
        guard let chat: Chat = self.realm.objects(Chat.self).filter(predicate).first,
            messageChatSchemas.first?.id != chat.lastMessage?.id else {
            return
        }
        let chatMessages: [MessageChat] = Array(messageChatSchemas.map{MessageChat.model(messageChatSchema:$0)}.prefix(while: {$0.id != chat.lastMessage?.id}))

        try? self.realm.write {
            self.realm.add(chatMessages, update: true)
            let lastMessage = chatMessages.first
            chat.lastMessage = lastMessage
            chat.updatedAt = lastMessage?.createdAt
            self.realm.add(chat, update: true)
            self.loadChats()
            self.chatMessagesEvent = Event.success(chatMessages)
        }
    }
}

// MARK: Save chat messages error
extension ChatRepository {
    func saveChatMessagesError(_ error: Error) {
        self.chatMessagesEvent = Event.error(error)
    }
}

// MARK: Unload chat messages
extension ChatRepository {
    func unloadChatMessages() {
        self.chatMessagesEvent = Event.error(MeetDayError.dbError(.emptyChatMessages))
        self.chatMessageSentEvent = Event.error(MeetDayError.dbError(.emptyChatMessageSent))
    }
}

// MARK: Save chat message sent
extension ChatRepository {
    func saveChatMessageSent(_ messageChatSchema: MessageChatSchema) {
        let chatId = messageChatSchema.chatId
        let predicate = NSPredicate(format: "id == %@", chatId)
        guard let chat: Chat = self.realm.objects(Chat.self).filter(predicate).first else {
                return
        }
        let chatMessage: MessageChat = MessageChat.model(messageChatSchema: messageChatSchema)
        
        try? self.realm.write {
            self.realm.add(chatMessage, update: true)
            chat.lastMessage = chatMessage
            chat.updatedAt = chatMessage.createdAt
            self.realm.add(chat, update: true)
            self.loadChats()
            self.chatMessageSentEvent = Event.success(chatMessage)
        }
    }
}

// MARK: Save chat message sent error
extension ChatRepository {
    func saveChatMessageSentError(_ error: Error) {
        self.chatMessageSentEvent = Event.error(error)
    }
}

// MARK: Restart events
extension ChatRepository {
    func restartEvents() {
        self.chatsEvent = Event.error(MeetDayError.dbError(.emptyChats))
        self.chatMessagesEvent = Event.error(MeetDayError.dbError(.emptyChatMessages))
        self.chatMessageSentEvent = Event.error(MeetDayError.dbError(.emptyChatMessageSent))
    }
}

// MARK: Private methods
extension ChatRepository {
    private func loadChats() {
        let chats: [Chat] = self.realm.objects(Chat.self).sorted(by: { (chat1, chat2) -> Bool in
            guard let updatedAt1 = chat1.updatedAt, let updatedAt2 = chat2.updatedAt else {
                return false
            }
            return updatedAt1 > updatedAt2
        })
        if !chats.isEmpty {
            self.chatsEvent = Event.success(chats)
        } else {
            self.chatsEvent = Event.error(MeetDayError.dbError(.emptyChats))
        }
    }
}
