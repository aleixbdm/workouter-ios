//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift
import ReactiveSwift

protocol RealmRepositoryProtocol: class {
    var profileRepository: ProfileRepository { get }
    var candidateRepository: CandidateRepository { get }
    var chatRepository: ChatRepository { get }
    func deleteAll()
}

class RealmRepository: RealmRepositoryProtocol {
    private let realm: Realm
    private(set) var profileRepository: ProfileRepository
    private(set) var candidateRepository: CandidateRepository
    private(set) var chatRepository: ChatRepository
    
    init() {
        self.realm = try! Realm() // Force to crash by now
        Logger.log("Database fileURL: \(self.realm.configuration.fileURL?.absoluteString ?? "Not found")")

        self.profileRepository = ProfileRepository(realm: self.realm)
        self.candidateRepository = CandidateRepository(realm: self.realm)
        self.chatRepository = ChatRepository(realm: self.realm)
    }
    
    // MARK: Delete all
    func deleteAll() {
        try? self.realm.write {
            self.realm.deleteAll()
            self.candidateRepository.restartEvents()
            self.chatRepository.restartEvents()
            self.profileRepository.restartEvents()
        }
    }
}
