//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift
import ReactiveSwift

protocol ProfileRepositoryProtocol: class {
    var profileProperty: MutableProperty<EventProperty<Profile>> { get }
    init(realm: Realm)
    func loadProfile()
    func saveProfile(_ profileSchema: ProfileSchema)
    func saveProfileError(_ error: Error)
    func restartEvents()
}

class ProfileRepository: ProfileRepositoryProtocol {
    private let realm: Realm
    private var profileEvent: Event<Profile> {
        didSet {
            self.profileProperty.value = EventProperty(event: self.profileEvent)
        }
    }
    private(set) var profileProperty: MutableProperty<EventProperty<Profile>>
    
    required init(realm: Realm) {
        self.realm = realm
        self.profileEvent = Event.error(MeetDayError.dbError(.emptyProfile))
        self.profileProperty = MutableProperty(EventProperty(event: self.profileEvent))

        self.loadProfile()
    }
    
    // MARK: Load profile
    func loadProfile() {
        if let profile: Profile = self.realm.objects(Profile.self).first {
            self.profileEvent = Event.success(profile)
        } else {
            self.profileEvent = Event.error(MeetDayError.dbError(.emptyProfile))
        }
    }
    
    // MARK: Save profile
    func saveProfile(_ profileSchema: ProfileSchema) {
        let profile = Profile.model(profileSchema: profileSchema)
        try? self.realm.write {
            self.realm.add(profile, update: true)
            self.profileEvent = Event.success(profile)
        }
    }
    
    // MARK: Save profile error
    func saveProfileError(_ error: Error) {
        self.profileEvent = Event.error(error)
    }
    
    // MARK: Restart events
    func restartEvents() {
        self.profileEvent = Event.error(MeetDayError.dbError(.emptyProfile))
    }
}
