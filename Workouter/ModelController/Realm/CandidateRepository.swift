//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift
import ReactiveSwift

protocol CandidateRepositoryProtocol: class {
    var candidatesProperty: MutableProperty<EventProperty<[Candidate]>> { get }
    var candidateProfileProperty: MutableProperty<EventProperty<Profile>> { get }
    var evaluationProperty: MutableProperty<EventProperty<Evaluation>> { get }
    init(realm: Realm)
    func saveCandidates(_ candidateSchemas: [CandidateSchema])
    func saveCandidatesError(_ error: Error)
    func saveCandidateProfile(_ profileSchema: ProfileSchema)
    func saveCandidateProfileError(_ error: Error)
    func unloadCandidateProfile()
    func saveEvaluation(_ evaluationSchema: EvaluationSchema)
    func saveEvaluationError(_ error: Error)
    func restartEvents()
}

final class CandidateRepository: CandidateRepositoryProtocol {
    private let realm: Realm
    private var candidatesEvent: Event<[Candidate]> {
        didSet {
            self.candidatesProperty.value = EventProperty(event: self.candidatesEvent)
        }
    }
    private var candidateProfileEvent: Event<Profile> {
        didSet {
            self.candidateProfileProperty.value = EventProperty(event: self.candidateProfileEvent)
        }
    }
    private var evaluationEvent: Event<Evaluation> {
        didSet {
            self.evaluationProperty.value = EventProperty(event: self.evaluationEvent)
        }
    }
    private(set) var candidatesProperty: MutableProperty<EventProperty<[Candidate]>>
    private(set) var candidateProfileProperty: MutableProperty<EventProperty<Profile>>
    private(set) var evaluationProperty: MutableProperty<EventProperty<Evaluation>>
    
    init(realm: Realm) {
        self.realm = realm
        self.candidatesEvent = Event.error(MeetDayError.dbError(.emptyCandidates))
        self.candidatesProperty = MutableProperty(EventProperty(event: self.candidatesEvent))
        
        self.candidateProfileEvent = Event.error(MeetDayError.dbError(.emptyCandidateProfile))
        self.candidateProfileProperty = MutableProperty(EventProperty(event: self.candidateProfileEvent))
        
        self.evaluationEvent = Event.error(MeetDayError.dbError(.emptyEvaluation))
        self.evaluationProperty = MutableProperty(EventProperty(event: self.evaluationEvent))
    }
}

// MARK: Save candidates
extension CandidateRepository {
    func saveCandidates(_ candidateSchemas: [CandidateSchema]) {
        let candidates: [Candidate] = candidateSchemas.map{Candidate.model(candidateSchema:$0)}
        self.candidatesEvent = Event.success(candidates)
    }
}

// MARK: Save candidates error
extension CandidateRepository {
    func saveCandidatesError(_ error: Error) {
        self.candidatesEvent = Event.error(error)
    }
}

// MARK: Save candidate profile
extension CandidateRepository {
    func saveCandidateProfile(_ profileSchema: ProfileSchema) {
        self.candidateProfileEvent = Event.success(Profile.model(profileSchema: profileSchema))
    }
}

// MARK: Save candidate profile error
extension CandidateRepository {
    func saveCandidateProfileError(_ error: Error) {
        self.candidateProfileEvent = Event.error(error)
    }
}

// MARK: Save candidate profile error
extension CandidateRepository {
    func unloadCandidateProfile() {
        self.candidateProfileEvent = Event.error(MeetDayError.dbError(.emptyCandidateProfile))
    }
}

// MARK: Save evaluation
extension CandidateRepository {
    func saveEvaluation(_ evaluationSchema: EvaluationSchema) {
        self.evaluationEvent = Event.success(Evaluation.model(evaluationSchema: evaluationSchema))
    }
}

// MARK: Save evaluation error
extension CandidateRepository {
    func saveEvaluationError(_ error: Error) {
        self.evaluationEvent = Event.error(error)
    }
}

// MARK: Restart events
extension CandidateRepository {
    func restartEvents() {
        self.candidatesEvent = Event.error(MeetDayError.dbError(.emptyCandidates))
        self.candidateProfileEvent = Event.error(MeetDayError.dbError(.emptyCandidateProfile))
        self.evaluationEvent = Event.error(MeetDayError.dbError(.emptyEvaluation))
    }
}
