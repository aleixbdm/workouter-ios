//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

protocol ModelControllerProtocol: class {
    var repository: RealmRepository { get }
    var userDefaults: UserDefaultsRepository { get }
}

class ModelController: ModelControllerProtocol {
    private(set) var repository: RealmRepository
    private(set) var userDefaults: UserDefaultsRepository
    
    init() {
        self.repository = RealmRepository()
        self.userDefaults = UserDefaultsRepository()
    }
}
