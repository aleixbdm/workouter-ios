//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import ObjectMapper
@testable import MeetDay

internal class TestValue<T> {
    private let defaultValue: T?
    private var _value: T?
    
    fileprivate init(_ defaultValue: T?) {
        self.defaultValue = defaultValue
        self._value = defaultValue
    }
    
    func value() -> T? {
        let value = self._value
        self._value = defaultValue
        return value
    }
    
    func set(_ value: T?) {
        self._value = value
    }
}

class TestValues {
    static var shared = TestValues()

    private init() {}

    // MARK: -- Profile Controller --
    static let email = "email@test.com"
    static let password = "test123"
    static let fbAccessToken = "fbAccessTokenTest"
    static let name = "TestName"
    static let firstName = "TestFirstName"
    static let imageUrls = ["TestImageUrl"]
    static let age = 18
    static let gender = Gender.male
    static let preference = Preference.all
    static let city = "CityTest"
    
    let savedAuth = TestValue<AuthSchema>(nil)
    let loadedProfile = TestValue<Profile>(nil)
    let savedProfile = TestValue<ProfileSchema>(nil)
    let savedSettings = TestValue<SettingsSchema>(nil)
    let savedProfileError = TestValue<Error>(nil)

    // MARK: Auth
    let auth = TestValue<Auth>(Auth(accessToken: "accessTokenTest", userId: "userIdTest"))
    
    // MARK: Settings
    let settings = TestValue<Settings>(Settings.model(profileId: "profileIdTest", visible: true, yearRangeMax: MaxAge, yearRangeMin: MinAge, searchRange: 120, city: "cityTest", preference: Preference.all.rawValue))

    // MARK: Profile
    let profile = TestValue<Profile>(Profile.model(profileSchema: try! ProfileSchema(map: Map(mappingType: MappingType.fromJSON, JSON: ["id": "loadedProfileIdTest"]))))
    
    // MARK: Login
    let loginParams = TestValue<NetworkParameters>(nil)
    let loginResponse = TestValue<AuthResponse>(try? AuthResponse(map: Map(mappingType: MappingType.fromJSON, JSON: ["auth": ["access_token": "loginAccessTokenTest", "user_id": "loginUserIdTest" ]])))
    let loginError = TestValue<Error>(MeetDayError.apiError(message: "loginError"))
    
    // MARK: Login with fb
    let loginWithFBParams = TestValue<NetworkParameters>(nil)
    let loginWithFBResponse = TestValue<AuthResponse>(try? AuthResponse(map: Map(mappingType: MappingType.fromJSON, JSON: ["auth": ["access_token": "loginWithFBAccessTokenTest", "user_id": "loginWithFBUserIdTest" ]])))
    let loginWithFBError = TestValue<Error>(MeetDayError.apiError(message: "loginWithFBError"))
    
    // MARK: Register
    let registerParams = TestValue<NetworkParameters>(nil)
    let registerResponse = TestValue<AuthResponse>(try? AuthResponse(map: Map(mappingType: MappingType.fromJSON, JSON: ["auth": ["access_token": "registerAccessTokenTest", "user_id": "registerUserIdTest" ]])))
    let registerError = TestValue<Error>(MeetDayError.apiError(message: "registerError"))
    
    // MARK: Fetch profile
    let fetchProfileResponse = TestValue<ProfileResponse>(try? ProfileResponse(map: Map(mappingType: MappingType.fromJSON, JSON: ["user": ["id": "fetchProfileIdTest", "settings": ["city": "fetchProfileCityTest"]]])))
    let fetchProfileError = TestValue<Error>(MeetDayError.apiError(message: "fetchProfileError"))
    
    // MARK: Create profile
    let createProfileParams = TestValue<NetworkParameters>(nil)
    let createProfileResponse = TestValue<ProfileResponse>(try? ProfileResponse(map: Map(mappingType: MappingType.fromJSON, JSON: ["user": ["id": "createProfileIdTest"]])))
    let createProfileError = TestValue<Error>(MeetDayError.apiError(message: "createProfileError"))
}
