//  Copyright © 2017 Aleix Pellisa Cortiella. All rights reserved.

import XCTest
@testable import MeetDay

class ProfileControllerTests: XCTestCase {
    let mockModelController = MockModelController()
    let mockNetworkController = MockNetworkController()
    var profileController: ProfileController? = nil
    
    override func setUp() {
        super.setUp()
        self.profileController = ProfileController(modelController: self.mockModelController, networkController: self.mockNetworkController)
    }
    
    // MARK: Auth
    func testAuth() {
        let expected = TestValues.shared.auth.value()
        let actual = self.profileController?.auth

        XCTAssertEqual(actual?.userId, expected?.userId)
        XCTAssertEqual(actual?.accessToken, expected?.accessToken)
    }
    
    // MARK: Settings
    func testSettings() {
        let expected = TestValues.shared.settings.value()
        let actual = self.profileController?.settings
        
        XCTAssertEqual(actual?.city, expected?.city)
    }

    // MARK: Login
    func testLoginParamsForwardedCorrectly() {
        let email = TestValues.email
        let password = TestValues.password
        self.profileController?.login(email: email, password: password)
        let actual = TestValues.shared.loginParams.value()

        XCTAssertEqual(actual?[.body, .email(DefaultString)], email)
        XCTAssertEqual(actual?[.body, .password(DefaultString)], password)
    }

    func testLoginWithGivenResponse() {
        let expected = TestValues.shared.loginResponse.value()?.authSchema
        TestValues.shared.loginError.set(nil)
        self.profileController?.login(email: TestValues.email, password: TestValues.password)
        let actual = TestValues.shared.savedAuth.value()

        XCTAssertEqual(actual?.userId, expected?.userId)
        XCTAssertEqual(actual?.accessToken, expected?.accessToken)
    }
    
    func testLoginWithGivenError() {
        let expected = TestValues.shared.loginError.value()
        TestValues.shared.loginResponse.set(nil)
        self.profileController?.login(email: TestValues.email, password: TestValues.password)
        let actual = TestValues.shared.savedProfileError.value()
        
        XCTAssertTrue(actual is MeetDayError)
        XCTAssertEqual(actual?.localizedDescription, expected?.localizedDescription)
    }
    
    // MARK: Login with fb
    func testLoginWithFBParamsForwardedCorrectly() {
        let fbAccessToken = TestValues.fbAccessToken
        self.profileController?.loginWithFB(accessToken: fbAccessToken)
        let actual = TestValues.shared.loginWithFBParams.value()
        
        XCTAssertEqual(actual?[.body, .accessToken(DefaultString)], fbAccessToken)
    }
    
    func testLoginWithFBWithGivenResponse() {
        let expected = TestValues.shared.loginWithFBResponse.value()?.authSchema
        TestValues.shared.loginWithFBError.set(nil)
        self.profileController?.loginWithFB(accessToken: TestValues.fbAccessToken)
        let actual = TestValues.shared.savedAuth.value()
        
        XCTAssertEqual(actual?.userId, expected?.userId)
        XCTAssertEqual(actual?.accessToken, expected?.accessToken)
    }
    
    func testLoginWithFBWithGivenError() {
        let expected = TestValues.shared.loginWithFBError.value()
        TestValues.shared.loginWithFBResponse.set(nil)
        self.profileController?.loginWithFB(accessToken: TestValues.fbAccessToken)
        let actual = TestValues.shared.savedProfileError.value()
        
        XCTAssertTrue(actual is MeetDayError)
        XCTAssertEqual(actual?.localizedDescription, expected?.localizedDescription)
    }
    
    // MARK: Register
    func testRegisterParamsForwardedCorrectly() {
        let email = TestValues.email
        let password = TestValues.password
        self.profileController?.register(email: email, password: password)
        let actual = TestValues.shared.registerParams.value()
        
        XCTAssertEqual(actual?[.body, .email(DefaultString)], email)
        XCTAssertEqual(actual?[.body, .password(DefaultString)], password)
    }
    
    func testRegisterWithGivenResponse() {
        let expected = TestValues.shared.registerResponse.value()?.authSchema
        TestValues.shared.registerError.set(nil)
        self.profileController?.register(email: TestValues.email, password: TestValues.password)
        let actual = TestValues.shared.savedAuth.value()
        
        XCTAssertEqual(actual?.userId, expected?.userId)
        XCTAssertEqual(actual?.accessToken, expected?.accessToken)
    }
    
    func testRegisterWithGivenError() {
        let expected = TestValues.shared.registerError.value()
        TestValues.shared.registerResponse.set(nil)
        self.profileController?.register(email: TestValues.email, password: TestValues.password)
        let actual = TestValues.shared.savedProfileError.value()
        
        XCTAssertTrue(actual is MeetDayError)
        XCTAssertEqual(actual?.localizedDescription, expected?.localizedDescription)
    }
    
    // MARK: Fetch profile
    func testLoadsProfileBeforeFetchProfile() {
        let expected = TestValues.shared.profile.value()
        self.profileController?.fetchProfile()
        let actual = TestValues.shared.loadedProfile.value()
        
        XCTAssertEqual(actual?.id, expected?.id)
    }
    
    func testFetchProfileWithGivenResponse() {
        let expected = TestValues.shared.fetchProfileResponse.value()?.profileSchema
        TestValues.shared.fetchProfileError.set(nil)
        self.profileController?.fetchProfile()
        let actual = TestValues.shared.savedProfile.value()
        
        XCTAssertEqual(actual?.id, expected?.id)
    }
    
    func testFetchProfileWithGivenError() {
        let expected = TestValues.shared.fetchProfileError.value()
        TestValues.shared.fetchProfileResponse.set(nil)
        self.profileController?.fetchProfile()
        let actual = TestValues.shared.savedProfileError.value()
        
        XCTAssertTrue(actual is MeetDayError)
        XCTAssertEqual(actual?.localizedDescription, expected?.localizedDescription)
    }
    
    func testFetchProfileWithSettingsSavesThem() {
        let expected = TestValues.shared.fetchProfileResponse.value()?.profileSchema?.settings
        TestValues.shared.fetchProfileError.set(nil)
        self.profileController?.fetchProfile()
        let actual = TestValues.shared.savedSettings.value()
        
        XCTAssertEqual(actual?.city, expected?.city)
    }
    
    // MARK: Create profile
    func testCreateProfileParamsForwardedCorrectly() {
        let name = TestValues.name
        let firstName = TestValues.firstName
        let imageUrls = TestValues.imageUrls
        let age = TestValues.age
        let gender = TestValues.gender
        let preference = TestValues.preference
        let city = TestValues.city
        self.profileController?.createProfile(name: name, firstName: firstName, imageUrls: imageUrls, age: age, gender: gender, preference: preference, city: city)
        let actual = TestValues.shared.createProfileParams.value()
        
        XCTAssertEqual(actual?[.body, .name(DefaultString)], name)
        XCTAssertEqual(actual?[.body, .firstName(DefaultString)], firstName)
        let actualImageUrls: [String] = (actual?[.body, .imageUrls(DefaultArrayString)])!
        XCTAssertEqual(actualImageUrls, imageUrls)
        XCTAssertEqual(actual?[.body, .age(DefaultInt)], age)
        XCTAssertEqual(actual?[.body, .gender(DefaultString)], gender.rawValue)
        XCTAssertEqual(actual?[.body, .preference(DefaultString)], preference.rawValue)
        XCTAssertEqual(actual?[.body, .city(DefaultString)], city)
    }
    
    func testCreateProfileWithGivenResponse() {
        let expected = TestValues.shared.createProfileResponse.value()?.profileSchema
        TestValues.shared.createProfileError.set(nil)
        self.profileController?.createProfile(name: TestValues.name, firstName: TestValues.firstName, imageUrls: TestValues.imageUrls, age: TestValues.age, gender: TestValues.gender, preference: TestValues.preference, city: TestValues.city)
        let actual = TestValues.shared.savedProfile.value()
        
        XCTAssertEqual(actual?.id, expected?.id)
    }
    
    func testCreateProfileWithGivenError() {
        let expected = TestValues.shared.createProfileError.value()
        TestValues.shared.createProfileResponse.set(nil)
        self.profileController?.createProfile(name: TestValues.name, firstName: TestValues.firstName, imageUrls: TestValues.imageUrls, age: TestValues.age, gender: TestValues.gender, preference: TestValues.preference, city: TestValues.city)
        let actual = TestValues.shared.savedProfileError.value()
        
        XCTAssertTrue(actual is MeetDayError)
        XCTAssertEqual(actual?.localizedDescription, expected?.localizedDescription)
    }
    
    // MARK: Logout
    func testLogoutDeleteUserDefaults() {
        TestValues.shared.savedAuth.set(AuthSchema(auth: TestValues.shared.auth.value()!))
        TestValues.shared.savedSettings.set(SettingsSchema(settings: TestValues.shared.settings.value()!))
        self.profileController?.logout()
        let savedAuth = TestValues.shared.savedAuth.value()
        let savedSettings = TestValues.shared.savedSettings.value()
        
        XCTAssertNil(savedAuth)
        XCTAssertNil(savedSettings)
    }
    
    func testLogoutDeleteRepository() {
        TestValues.shared.savedProfile.set(ProfileSchema(profile: TestValues.shared.profile.value()!)!)
        self.profileController?.logout()
        let savedProfile = TestValues.shared.savedProfile.value()
        
        XCTAssertNil(savedProfile)
    }
}
