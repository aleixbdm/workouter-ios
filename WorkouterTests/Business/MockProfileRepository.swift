//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift
@testable import MeetDay

class MockProfileRepository: ProfileRepository {
    override func loadProfile() {
        TestValues.shared.loadedProfile.set(TestValues.shared.profile.value())
    }
    
    override func saveProfile(_ profileSchema: ProfileSchema) {
        TestValues.shared.savedProfile.set(profileSchema)
    }
    
    override func saveProfileError(_ error: Error) {
        TestValues.shared.savedProfileError.set(error)
    }
}
