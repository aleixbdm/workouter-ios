//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

import RealmSwift
@testable import MeetDay

class MockRealmRepository: RealmRepository {
    override var profileRepository: ProfileRepository {
        return MockProfileRepository(realm: try! Realm())
    }
    
    override func deleteAll() {
        TestValues.shared.savedProfile.set(nil)
    }
}

