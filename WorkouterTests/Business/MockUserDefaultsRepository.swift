//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

@testable import MeetDay

class MockUserDefaultsRepository: UserDefaultsRepository {
    override var auth: Auth? {
        return TestValues.shared.auth.value()
    }
    
    override var settings: Settings? {
        return TestValues.shared.settings.value()
    }
    
    override func saveAuth(_ authSchema: AuthSchema) {
        TestValues.shared.savedAuth.set(authSchema)
    }
    
    override func saveSettings(_ settingsSchema: SettingsSchema) {
        TestValues.shared.savedSettings.set(settingsSchema)
    }
    
    override func deleteAll() {
        TestValues.shared.savedAuth.set(nil)
        TestValues.shared.savedSettings.set(nil)
    }
}
