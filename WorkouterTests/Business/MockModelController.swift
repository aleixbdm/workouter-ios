//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

@testable import MeetDay

class MockModelController: ModelController {
    override var repository: RealmRepository {
        return MockRealmRepository()
    }
    
    override var userDefaults: UserDefaultsRepository {
        return MockUserDefaultsRepository()
    }
}
