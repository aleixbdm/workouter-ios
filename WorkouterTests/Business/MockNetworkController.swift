//  Copyright © 2018 Aleix Pellisa Cortiella. All rights reserved.

@testable import MeetDay

class MockNetworkController: NetworkController {
    override func login(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()) {
        TestValues.shared.loginParams.set(params)
        completion(TestValues.shared.loginResponse.value(), TestValues.shared.loginError.value())
    }
    
    override func loginWithFB(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()) {
        TestValues.shared.loginWithFBParams.set(params)
        completion(TestValues.shared.loginWithFBResponse.value(), TestValues.shared.loginWithFBError.value())
    }
    
    override func register(params: NetworkParameters?, completion: @escaping (AuthResponse?, Error?) -> ()) {
        TestValues.shared.registerParams.set(params)
        completion(TestValues.shared.registerResponse.value(), TestValues.shared.registerError.value())
    }
    
    override func fetchProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()) {
        completion(TestValues.shared.fetchProfileResponse.value(), TestValues.shared.fetchProfileError.value())
    }
    
    override func createProfile(params: NetworkParameters?, completion: @escaping (ProfileResponse?, Error?) -> ()) {
        TestValues.shared.createProfileParams.set(params)
        completion(TestValues.shared.createProfileResponse.value(), TestValues.shared.createProfileError.value())
    }
}
